@extends('layout') 
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Table jsgrid</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item">pages</li>
            <li class="breadcrumb-item active">Table jsgrid</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">            
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Categorias Usuario</h4>
                    <a href="{{ route('categoriasu.create') }}" class="btn btn-primary m-b-20 p-10 btn-block waves-effect waves-light">Crear</a>
                    <div class="table-responsive m-t-10">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Creado</th>
                                    <th width="20">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>                             
                                @foreach($categorias as $categoria)
                                <tr>
                                    <td>{{ $categoria->id }}</td>
                                    <td>{{ $categoria->nombre_cc }}</td>
                                    <td>{{ $categoria->format_fecha }}</td>
                                    <td width="20">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <a href="{{ route('categoriasu.show', $categoria) }}" class="btn btn-secondary font-18"><i class="fa fa-file-text-o"></i></a>

                                            <a href="{{ route('categoriasu.edit', $categoria) }}" class="btn btn-secondary font-18"><i class="fa fa-edit"></i></a>                                       

                                            <form method="POST" action="{{ route('categoriasu.destroy', $categoria) }}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" value="Eliminar" class="btn btn-secondary font-18" onclick="return confirm('Desea eliminar..?')"><i class="mdi mdi-delete"></i></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });            
        });
    });  
</script>
@endsection    