@extends('layout') 
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Crear Categoria Usuario</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Categorias</a></li>
            <li class="breadcrumb-item active">Nueva categorias</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="container-fluid">
    <div class="row"> 
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Formulario Crear</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('categoriasu.store') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-body">
                            <h3 class="box-title">Ingrese los siguiente datos</h3>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Nombre</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="Ingrese el nombre" name="nombre_cc" required>
                                            <small class="form-control-feedback">Ejemplo: comercio ó publicidad</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            @csrf
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                            <a href="{{route('categoriasu.index')}}" class="btn btn-inverse">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"> </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection