@extends('layout') 
@section('content') 

<div class="container-fluid m-t-20">
    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-lg-12">
            @if(Auth::user()->cc_id == 3)
            <form action="{{ route('configuracion.update', $configuracion) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            @endif
                <div class="card card-outline-info">              
                    <div class="card-body">
                        <div class="form-body">
                            @if(Auth::user()->cc_id == 3)
                            <h3 class="box-title">CONFIGURACIONES</h3>
                            <hr class="m-t-0 m-b-40">                            
                            <div class="row">                            
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Banco</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="banco" value="{{ old('banco', $configuracion->banco) }}" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>                                 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4"># Cuenta</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="cuenta" value="{{ old('cuenta', $configuracion->cuenta) }}" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Tipo Cuenta</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="tipo_cuenta" required>
                                                <option value="Ahorros" @if( $configuracion->tipo_cuenta == 'Ahorros' ) {!! _('selected') !!} @endif >Ahorros</option>
                                                <option value="Corriente" @if( $configuracion->tipo_cuenta == 'Corriente' ) {!! _('selected') !!} @endif >Corriente</option>
                                            </select>
                                        </div>
                                    </div>      
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">%Comisión</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="comision" value="{{ old('comision', $configuracion->comision) }}">
                                        </div>
                                    </div>
                                    <!--
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Tiempo PRO</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="tiempo_link" value="{{ old('tiempo_link', $configuracion->tiempo_link) }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-time"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div> 
                                    -->
                                </div>
                            </div>
                            @else
                            <h3 class="box-title">DATOS BANCARIOS ADMIN</h3>
                            <hr class="m-t-0 m-b-40">                            
                            <div class="row">                            
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Banco</label>
                                        <div class="col-md-8 input-group">
                                            {{$configuracion->banco}}
                                        </div>
                                    </div>                                 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4"># Cuenta</label>
                                        <div class="col-md-8 input-group">
                                            {{$configuracion->cuenta}}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Tipo Cuenta</label>
                                        <div class="col-md-8">
                                            {{$configuracion->tipo_cuenta}}
                                        </div>
                                    </div>      
                                </div>                                
                            </div>
                            @endif
                        </div>
                    </div>                
                </div>
                @if(Auth::user()->cc_id == 3)
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            @method('PUT')                      
                            <button type="submit" class="btn-md btn-primary float-right  m-r-10">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
                @endif
        </div>
    </div>
    @if(Auth::user()->cc_id == 3)
    <br>
    <div class="row"> 
        <div class="col-lg-12">
            <div class="card card-outline-info">              
                <div class="card-body">
                    <div class="form-body">
                        <h3 class="box-title">
                            CATEGORIA PRODUCTOS
                        </h3>
                        <hr class="m-t-0 m-b-10">
                        <div>
                            <button type="button" class="btn-md btn-primary float-right m-r-10" id="btnAddCategory">Nueva categoría</button>
                        </div>
                        <div>
                            <table id="tblCategoriaProductos" class="table table-light elevation-1">
                                <thead class="thead-gray">
                                    <tr>
                                        <th style="width: 90%;">Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categoriaProductos as $cp)
                                    <tr cat-id="{{$cp->id}}">
                                        <td class="td-name">{{$cp->nombre_cp}}</td>
                                        <td class="text-center">
                                            @if($cp->productos_count == 0)
                                            <button type="button" class="font-18 only-btn" title="Eliminar {{$cp->nombre_cp}}" onclick="deleteCategory($(this));">
                                                <i class="fa fa-trash m-r-5"></i>
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    @endif
</div>

<div class="modal fade" id="createCategoriaProductoModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form name="categoariProductoCreateForm" id="categoariProductoCreateForm" method="POST" action="{{route('categoriaproductos.create')}}">
                <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                <div class="modal-header">
                    <h5 class="modal-title">Nueva categoría productos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: black !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="inventaryName">Nombre</label>
                                    <input type="text" class="form-control" id="categoriaProductoName" required="" maxlength="145">
                                    <span style="color :red;" class="error-create"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setEvents();
        setDataTable();
    });

    function setEvents() {
        $('#categoariProductoCreateForm').submit(function (e) {
            e.preventDefault();
            saveCategory($(this));
        });

        $('#btnAddCategory').click(function () {
            $("#createCategoriaProductoModal").modal();
        });

    }

    function saveCategory($form) {
        $form.find('.error-create').text('');
        $.post($form.attr('action'), {
            _token: $('[name="_token"]').val(),
            name: $form.find('#categoriaProductoName').val()
        }, function (response) {
            if (response.status) {
                window.location.reload();
            } else {
                $form.find('.error-create').text(response.message);
            }
        });
    }

    function deleteCategory($this) {
        var $trParent = $this.parents('tr:first');
        if (confirm('¿Realmente desea eliminar la categoría ' + $trParent.find('.td-name').text() + '?')) {
            $.post('/categoriaproducto/eliminar', {
                _token: $('[name="_token"]').val(),
                id: $trParent.attr('cat-id')
            }, function (response) {
                if (response.status) {
                    window.location.reload();
                    return;
                }
                alert(response.message);
            });
        }

    }

    function setDataTable() {
        $('#tblCategoriaProductos').dataTable({
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    }

</script>
@endsection