@extends('layout') 

@section('content')
@if (Route::has('login'))
<div class="top-right links" style="display:none">
    @auth
    <a href="{{ url('/home') }}">Home</a>
    @else
    <a href="{{ route('login') }}">Login</a>

    @if (Route::has('register'))
    <a href="{{ route('register') }}">Register</a>
    @endif
    @endauth
</div>
@endif
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles" style="display:none">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Dashboard4</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Dashboard4</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid p-t-10">        
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <h3 class="title">Estadísticas</h3>
        </div>

        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-4 align-self-center text-right p-l-15">
                           <svg width="17" height="29" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="17" height="29" fill="transparent" class="stencil__selection-helper"></rect><path d="M9.151277013752459 12.72777777777778C5.360510805500981 11.777222222222223 4.141453831041259 10.794444444444443 4.141453831041259 9.26388888888889 4.141453831041259 7.507777777777777 5.828094302554028 6.283333333333333 8.650294695481335 6.283333333333333 11.622789783889981 6.283333333333333 12.724950884086443 7.652777777777777 12.825147347740668 9.666666666666666 12.825147347740668 9.666666666666666 16.51571709233792 9.666666666666666 16.51571709233792 9.666666666666666 16.39882121807466 6.895555555555555 14.645383104125738 4.35 11.155206286836936 3.528333333333334 11.155206286836936 3.528333333333334 11.155206286836936 0 11.155206286836936 0 11.155206286836936 0 6.145383104125738 0 6.145383104125738 0 6.145383104125738 0 6.145383104125738 3.4800000000000004 6.145383104125738 3.4800000000000004 2.9056974459724962 4.156666666666666 0.30058939096267245 6.186666666666666 0.30058939096267245 9.29611111111111 0.30058939096267245 13.017777777777779 3.490176817288802 14.870555555555557 8.149312377210215 15.950000000000001 12.324165029469548 16.916666666666664 13.159135559921413 18.334444444444443 13.159135559921413 19.83277777777778 13.159135559921413 20.944444444444443 12.340864440078587 22.71666666666667 8.650294695481335 22.71666666666667 5.210216110019646 22.71666666666667 3.857563850687624 21.234444444444442 3.673870333988212 19.333333333333336 3.673870333988212 19.333333333333336 0 19.333333333333336 0 19.333333333333336 0.2003929273084495 22.86166666666667 2.9390962671905694 24.843333333333334 6.145383104125738 25.503888888888888 6.145383104125738 25.503888888888888 6.145383104125738 29 6.145383104125738 29 6.145383104125738 29 11.155206286836936 29 11.155206286836936 29 11.155206286836936 29 11.155206286836936 25.53611111111111 11.155206286836936 25.53611111111111 14.411591355599214 24.939999999999998 17 23.119444444444447 17 19.81666666666667 17 15.241111111111111 12.942043222003932 13.678333333333333 9.151277013752459 12.72777777777778 9.151277013752459 12.72777777777778 9.151277013752459 12.72777777777778 9.151277013752459 12.72777777777778" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(67, 40, 116)" fill-rule="evenOdd"></path></svg>
                        </div>
                        <div class="col-8">
                            <h2 class="description-dashboard">{{$ventas}}</h2>
                            <h6 class="title-dashboard">Dinero ventas</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-4 align-self-center text-right p-l-0">
                          <svg width="37" height="32" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="37" height="32" fill="transparent" class="stencil__selection-helper"></rect><path d="M34.94444444444444 0C34.94444444444444 0 2.0555555555555554 0 2.0555555555555554 0 2.0555555555555554 0 2.0555555555555554 4 2.0555555555555554 4 2.0555555555555554 4 34.94444444444444 4 34.94444444444444 4 34.94444444444444 4 34.94444444444444 0 34.94444444444444 0 34.94444444444444 0 34.94444444444444 0 34.94444444444444 0M37 20C37 20 37 16 37 16 37 16 34.94444444444444 6 34.94444444444444 6 34.94444444444444 6 2.0555555555555554 6 2.0555555555555554 6 2.0555555555555554 6 0 16 0 16 0 16 0 20 0 20 0 20 2.0555555555555554 20 2.0555555555555554 20 2.0555555555555554 20 2.0555555555555554 32 2.0555555555555554 32 2.0555555555555554 32 22.611111111111107 32 22.611111111111107 32 22.611111111111107 32 22.611111111111107 20 22.611111111111107 20 22.611111111111107 20 30.833333333333336 20 30.833333333333336 20 30.833333333333336 20 30.833333333333336 32 30.833333333333336 32 30.833333333333336 32 34.94444444444444 32 34.94444444444444 32 34.94444444444444 32 34.94444444444444 20 34.94444444444444 20 34.94444444444444 20 37 20 37 20 37 20 37 20 37 20M18.5 28C18.5 28 6.166666666666666 28 6.166666666666666 28 6.166666666666666 28 6.166666666666666 20 6.166666666666666 20 6.166666666666666 20 18.5 20 18.5 20 18.5 20 18.5 28 18.5 28 18.5 28 18.5 28 18.5 28" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(67, 40, 116)" fill-rule="evenOdd"></path></svg>
                        </div>
                        <div class="col-8">
                            <h2 class="description-dashboard">{{$visitas}}</h2>
                            <h6 class="title-dashboard">Clicks a web</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(Auth::user()->cc_id == 1)
        <!-- Column -->
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-3 align-self-center text-right p-l-15">
                            <svg width="37" height="35" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="37" height="35" fill="transparent" class="stencil__selection-helper"></rect><path d="M35.05263157894737 29.166666666666668C35.05263157894737 29.166666666666668 35.05263157894737 31.11111111111111 35.05263157894737 31.11111111111111 35.05263157894737 33.25 33.300000000000004 35 31.157894736842106 35 31.157894736842106 35 3.894736842105263 35 3.894736842105263 35 1.7437330796063844 35 0 33.25888513823086 0 31.11111111111111 0 31.11111111111111 0 3.8888888888888884 0 3.8888888888888884 0 1.7411148617691374 1.7437330796063844 0 3.894736842105263 0 3.894736842105263 0 31.157894736842106 0 31.157894736842106 0 33.300000000000004 0 35.05263157894737 1.75 35.05263157894737 3.8888888888888884 35.05263157894737 3.8888888888888884 35.05263157894737 5.833333333333333 35.05263157894737 5.833333333333333 35.05263157894737 5.833333333333333 17.526315789473685 5.833333333333333 17.526315789473685 5.833333333333333 15.375312026974807 5.833333333333333 13.631578947368421 7.5744481951024705 13.631578947368421 9.722222222222221 13.631578947368421 9.722222222222221 13.631578947368421 25.27777777777778 13.631578947368421 25.27777777777778 13.631578947368421 27.425551804897527 15.375312026974807 29.166666666666668 17.526315789473685 29.166666666666668 17.526315789473685 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668M17.526315789473685 25.27777777777778C17.526315789473685 25.27777777777778 37 25.27777777777778 37 25.27777777777778 37 25.27777777777778 37 9.722222222222221 37 9.722222222222221 37 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778M25.315789473684212 20.416666666666668C23.699473684210528 20.416666666666668 22.394736842105264 19.11388888888889 22.394736842105264 17.5 22.394736842105264 15.886111111111113 23.699473684210528 14.583333333333336 25.315789473684212 14.583333333333336 26.93210526315789 14.583333333333336 28.23684210526316 15.886111111111113 28.23684210526316 17.5 28.23684210526316 19.11388888888889 26.93210526315789 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(67, 40, 116)" fill-rule="evenOdd"></path></svg>
                        </div>
                        <div class="col-9">
                            <h2 class="description-dashboard">{{$comision}}</h2>
                            <h6 class="title-dashboard">Comisiones por pagar</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-3 align-self-center text-right p-l-15">
                            <svg width="37" height="35" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="37" height="35" fill="transparent" class="stencil__selection-helper"></rect><path d="M35.05263157894737 29.166666666666668C35.05263157894737 29.166666666666668 35.05263157894737 31.11111111111111 35.05263157894737 31.11111111111111 35.05263157894737 33.25 33.300000000000004 35 31.157894736842106 35 31.157894736842106 35 3.894736842105263 35 3.894736842105263 35 1.7437330796063844 35 0 33.25888513823086 0 31.11111111111111 0 31.11111111111111 0 3.8888888888888884 0 3.8888888888888884 0 1.7411148617691374 1.7437330796063844 0 3.894736842105263 0 3.894736842105263 0 31.157894736842106 0 31.157894736842106 0 33.300000000000004 0 35.05263157894737 1.75 35.05263157894737 3.8888888888888884 35.05263157894737 3.8888888888888884 35.05263157894737 5.833333333333333 35.05263157894737 5.833333333333333 35.05263157894737 5.833333333333333 17.526315789473685 5.833333333333333 17.526315789473685 5.833333333333333 15.375312026974807 5.833333333333333 13.631578947368421 7.5744481951024705 13.631578947368421 9.722222222222221 13.631578947368421 9.722222222222221 13.631578947368421 25.27777777777778 13.631578947368421 25.27777777777778 13.631578947368421 27.425551804897527 15.375312026974807 29.166666666666668 17.526315789473685 29.166666666666668 17.526315789473685 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668 35.05263157894737 29.166666666666668M17.526315789473685 25.27777777777778C17.526315789473685 25.27777777777778 37 25.27777777777778 37 25.27777777777778 37 25.27777777777778 37 9.722222222222221 37 9.722222222222221 37 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 9.722222222222221 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778 17.526315789473685 25.27777777777778M25.315789473684212 20.416666666666668C23.699473684210528 20.416666666666668 22.394736842105264 19.11388888888889 22.394736842105264 17.5 22.394736842105264 15.886111111111113 23.699473684210528 14.583333333333336 25.315789473684212 14.583333333333336 26.93210526315789 14.583333333333336 28.23684210526316 15.886111111111113 28.23684210526316 17.5 28.23684210526316 19.11388888888889 26.93210526315789 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668 25.315789473684212 20.416666666666668" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(67, 40, 116)" fill-rule="evenOdd"></path></svg>
                        </div>
                        <div class="col-9">
                            <h2 class="description-dashboard">{{$comision}}</h2>
                            <h6 class="title-dashboard">Comisiones por Recibir</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-wrap">
                        <div>
                            <h4 class="card-title title-dashboard">Ventas por mes</h4>
                        </div>
                    </div>
                    <div id="morris-area-chart2" style="height: 317px;"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-5">
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-wrap">
                        <div>
                            <h4 class="card-title title-dashboard">Productos mas vendidos</h4>
                        </div>
                    </div>
                    <div id="morris-area-chart3" style="height: 317px;"></div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title m-b-0 title-dashboard">Categoria de productos con mas ventas</h4>
                </div>
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="morris-donut-chart" class="ecomm-donute" style="height: 317px;"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="morris-area-chart5" class="ecomm-donute" style="height: 317px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <div class="right-sidebar">
        <div class="slimscrollright">
            <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
            <div class="r-panel-body">
                <ul id="themecolors" class="m-t-20">
                    <li><b>With Light sidebar</b></li>
                    <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                    <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                    <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                    <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme working">4</a></li>
                    <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                    <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                    <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                    <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                    <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                    <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                    <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                    <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                    <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                </ul>
                <ul class="m-t-20 chatonline">
                    <li><b>Chat option</b></li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="../assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->    
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->    
</div>

<script>
$(function () {
    "use strict";
// ==============================================================
    // Morris donut chart
    // ==============================================================
    if ($("#morris-donut-chart").length > 0) {
        Morris.Donut({
            element: "morris-donut-chart",
            colors: [
                '#432874',
                '#E4CE0B',
                '#E0F7FA',
                '#006064',
                '#0097A7'
            ],
            data: [
                @foreach ($topcategorias as $categoria)
                {
                    label: "{{$categoria->nombre_cp}}",
                    value: {{$categoria->numberSold}},
                },   
                @endforeach           
            ],
            resize: true,
        });

        var data = [            
            @foreach ($ventasmes as $ventames)
                { y: '{{$ventames->MES}}', a: {{$ventames->TOTAL_ACTIONS}} },
            @endforeach
        ];
        var config = {
            data: data,
            xkey: "y",
            ykeys: ["a"],
            labels: ["Total ventas"],
            fillOpacity: 0.6,
            hideHover: "auto",
            behaveLikeLine: true,
            resize: true,
            pointFillColors: ["#ffffff"],
            pointStrokeColors: ["black"],
            lineColors: ["#432874"],
        };
        config.barColors = ["#432874","#E4CE0B"];
        config.element = "morris-area-chart2";
        Morris.Bar(config);

        var dataProducts = [
            @foreach ($topproductos as $topproducto)
                {
                    y: "{{$topproducto->nombre_p}}",
                    a: {{$topproducto->numberSold}},
                },  
            @endforeach           
        ];
        config.data = dataProducts;
        config.horizontal = true;
        config.element = "morris-area-chart3";
        Morris.Bar(config);


        config.data = [
            @foreach ($topcategorias as $categoria)                
                { y: "{{$categoria->nombre_cp}}", a: {{$categoria->numberSold}} },
            @endforeach
        ];
        //config.barColors = undefined;
        config.element = "morris-area-chart5";
        Morris.Bar(config);
    }
});
</script>
<style>
    .title{
        color: rgb(67, 40, 116);
        font-size: 30px;
        font-weight: 400;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    .title-dashboard{
        font-size: 15px;
        color: rgb(67, 40, 116);
        font-weight: 600;
        text-transform: uppercase;
    }
    .description-dashboard{
        font-size: 30px;
        color: rgb(67, 40, 116);
        font-weight: 400;
    }

    .icon-dashboard{
        color: rgb(67, 40, 116);
        font-size: 30px;
    }
</style>
@endsection