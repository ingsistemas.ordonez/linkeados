@extends('layout') 
@section('content') 

<div class="container-fluid m-t-20">
    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-md-12">
            <h3 class="title">Bienvenido {{$user->name}}</h3>
        </div>
        <div class="col-lg-12">
            <form action="{{route('perfil.editprocess')}}" id="form-profile" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <div class="card card-outline-info">              
                    <div class="card-body">
                        <div class="form-body">
                            <h3 class="box-title title-edit">DATOS DE PERFIL</h3>
                            <hr class="m-t-0 m-b-40">                            
                            <div class="row">                            
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name" required maxlength="255" value="{{$user->name}}">                                            
                                        </div>
                                    </div>   
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Ciudad</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="ciudad" maxlength="145" value="{{$user->ciudad}}">                                            
                                        </div>
                                    </div>   
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Dirección</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="direccion" maxlength="145" value="{{$user->direccion}}">                                           
                                        </div>
                                    </div>   
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Teléfono</label>
                                        <div class="col-md-8">
                                            <input type="tel" class="form-control" name="telefono" maxlength="50" value="{{$user->telefono}}">                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Correo</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control" name="email" maxlength="100" value="{{$user->email}}">                                           
                                        </div>
                                    </div>
                                    @if(Auth::user()->cc_id != 2)
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Sitio web</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sitio_web" value="{{$user->sitio_web}}">
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">¿Quién es?</label>
                                        <div class="col-md-8">
                                            <textarea type="text" class="form-control" name="quien_es">{{$user->quien_es}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-logo-soporte">                                    
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Img. Perfil</label>
                                        <div class="col-md-8 input-group">
                                            @if($user)
                                            <div class="col-6 content-img">
                                                <a href="/storage/app/public/{{$user->url_perfil}}" target="_blank">
                                                    <img src="/storage/app/public/{{$user->url_perfil}}" style="width: 100%;margin-bottom: 5px;">
                                                </a>
                                            </div>
                                            @endif
                                            <input type="file" name="img_perfil" id="img_perfil" class="d-none" accept="image/x-png,image/gif,image/jpeg">
                                            <button type="button" class="btn btn-light btn-upload">Seleccionar archivo
                                                <i class="fa fa-camera float-right"></i>
                                            </button>
                                            <br/>
                                            <span style="color : red;" class="alert-upload d-none">Seleccione un archivo</span>
                                        </div>
                                    </div> 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Redes 1</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="redes1" value="{{$user->redes1}}">                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Redes 2</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="redes2" value="{{$user->redes2}}">                                           
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase label-form">Redes 3</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="redes3" value="{{$user->redes3}}">                                          
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <div class="spinner-border text-primary d-none loading-icon" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </div>                      
                        </div>
                    </div>                
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            @method('PUT')                      
                            <button type="submit" class="btn-md btn-primary float-right  m-r-10">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>

            @if(Auth::user()->cc_id == 1)
            <br/>
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Linkear</h4>
                </div>
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-md-12">
                            @if(!empty($user->url_pago))
                            <p>Copie la etiqueta a continuación y péguela entre las etiquetas &lt;head&gt;&lt;/head&gt; ó &lt;body&gt;&lt;/body&gt; de todas las páginas de su sitio web. Solo debe instalar la etiqueta global del sitio una vez por cuenta, incluso si hace un seguimiento de varias acciones.</p>

                            <div class="form-group row">                              
                                <pre class='code code-javascript'>
                                    <label>JS</label>
                                    <code id="code_copy">&lt;!-- The core Linkeado JS SDK is always required and must be listed first --&gt;
&lt;script src="https://app.linkiados.com/linkeadoscom.js"&gt;&lt;/script&gt;
&lt;script&gt;
  // Your web app's Linkeados configuration
  var linkeadoConfig = {   
    vendorEmail: "{{$user->email}} ",
    urlpay: "{{$user->url_pago}}"
  };
  // Initialize Linkeados
  linkeadosApp(linkeadoConfig);
&lt;/script&gt;
&lt;script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP">&lt;/script&gt;
</code>
                                <div class="copiado">Copiado</div>
                                </pre>  
                            </div>
                            @else
                            <p>Debe ingresar la url de pago de su sitio web</p>
                            @endif

                            <form action="{{ route('users.updateurl', $user) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-2">URL Conversión</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="url_pago" value="{{$user->url_pago}}" required>                                      
                                    </div>
                                    <div class="col-md-2">
                                        @csrf
                                        <button type="submit" class="btn-md btn-primary float-right  m-r-10">Guardar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if(Auth::user()->cc_id == 1)
            <br/>
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Shopify</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($shopify)
                            <form action="{{ route('perfil.setshopify') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="control-label text-right">Nombre tienda</label>
                                        <input type="text" class="form-control" name="nombre" value="{{$shopify->nombre}}" required>               
                                        <span>Ejem: linkeadotienda</span>     
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label  text-right">Verificado:</label>
                                        @if($shopify->verificado == 'NO')
                                        <div class="alert alert-warning" role="alert">
                                            <b>La integración aún no esta verificado</b>, por favor desde su tienda envie una notificación de prueba en el evento de Pago de pedido / Order payment y dale click <a href="{{route('perfil.edit')}}">aquí</a> para verificar.
                                        </div>
                                        @else
                                        <div class="alert alert-success" role="alert">
                                            <b>La integración esta verificado.</b>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="control-label text-right">URL tienda</label>
                                        <input type="text" class="form-control" name="url_tienda" value="{{$shopify->url_tienda}}" required>               
                                        <span>Ejem: https://linkeadotienda.myshopify.com/products/</span>                                                     
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label text-right">Key Webhooks</label>
                                        <input type="text" class="form-control" name="llave" value="{{$shopify->llave}}" required>      
                                        <a href="javascript: void(0)">Ver instrucciones en video para la conexión de Shopify</a>  <br/>
                                        <a href="javascript: void(0)" class="btnShow">Ver las instrucciones para creación del WebHook</a>   
                                        <br/> 
                                        <div class="card contentURL hidden p-10 m-t-10">
                                            <h5 class="">Events</h5>
                                            <p><b>Creación de carrito / Created Cart:</b><br/>
                                            https://app.linkiados.com/api/webhook/tiendashopify</br></p>
                                            <p><b>Actualización del carrito / Updated Cart:</b><br/> 
                                            https://app.linkiados.com/api/webhook/tiendashopify</p>	
                                            <p><b>Pago de pedido / Order payment:</b><br/>
                                            https://app.linkiados.com/api/webhook/pagoshopify</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        @csrf
                                        <button type="submit" class="btn-md btn-primary float-right  m-r-10">Guardar</button>
                                    </div>
                                </div>
                            </form>
                            @else
                            <form action="{{ route('perfil.setshopify') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="control-label text-right">Nombre tienda</label>
                                        <input type="text" class="form-control" name="nombre" value="" required>               
                                        <span>Ejem: linkeadotienda</span>                                                     
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="control-label text-right">URL tienda</label>
                                        <input type="text" class="form-control" name="url_tienda" value="" required>               
                                        <span>Ejem: https://linkeadotienda.myshopify.com/products/</span>                                                     
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label text-right">Key Webhooks</label>
                                        <input type="text" class="form-control" name="llave" value="" required>       
                                        <a href="javascript: void(0)">Ver instrucciones en video para la conexión de Shopify</a>  <br/>
                                        <a href="javascript: void(0)" class="btnShow">Ver las instrucciones para creación del WebHook</a>   
                                        <br/> 
                                        <div class="card contentURL hidden p-10 m-t-10">
                                            <h5 class="">Events</h5>
                                            <p><b>Creación de carrito / Created Cart:</b><br/>
                                            https://app.linkiados.com/api/webhook/tiendashopify</br></p>
                                            <p><b>Actualización del carrito / Updated Cart:</b><br/> 
                                            https://app.linkiados.com/api/webhook/tiendashopify</p>	
                                            <p><b>Pago de pedido / Order payment:</b><br/>
                                            https://app.linkiados.com/api/webhook/pagoshopify</p>
                                        </div>     
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        @csrf
                                        <button type="submit" class="btn-md btn-primary float-right  m-r-10">Guardar</button>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<style>
    .label-form{
        font-weight: 600;
        font-size: 15px;
    }

    .title-edit{
        font-size: 23px;
        font-weight: bold;
        color : rgb(67, 40, 116);
    }

    .btn-upload{
        background-color: white;
        border: 1px solid gray;
        border-radius: 5px;
        width: 100%;
    }

    .title{
        color: rgb(67, 40, 116);
        font-size: 30px;
        font-weight: 400;
        margin-bottom: 30px;
        margin-top: 30px;
    }

/* CSS Simple Pre Code */
    pre {
        background: #333;
        white-space: pre;
        word-wrap: break-word;
        overflow: auto;
    }

    pre.code {
        margin: 20px 25px;
        border-radius: 4px;
        border: 1px solid #292929;
        position: relative;
        width: 100%;
    }

    pre.code label {
        font-family: sans-serif;
        font-weight: bold;
        font-size: 13px;
        color: #ddd;
        position: absolute;
        left: 1px;
        top: 15px;
        text-align: center;
        width: 60px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        pointer-events: none;
    }

    pre.code code {
        font-family: "Inconsolata","Monaco","Consolas","Andale Mono","Bitstream Vera Sans Mono","Courier New",Courier,monospace;
        display: block;
        margin: 0 0 0 60px;
        padding: 15px 16px 14px;
        border-left: 1px solid #555;
        overflow-x: auto;
        font-size: 13px;
        line-height: 19px;
        color: #E2CC0B;
    }

    pre::after {
        content: "copy";
        padding: 0;
        width: auto;
        height: auto;
        position: absolute;
        right: 18px;
        top: 14px;
        font-size: 12px;
        color: #ddd;
        line-height: 20px;
        overflow: hidden;
        -webkit-backface-visibility: hidden;
        transition: all 0.3s ease;
    }

    pre:hover::after {
    color: red;
    }

    pre.code-javascript code {
        color: #E2CC0B;
    }

    pre.code-jquery code {
        color: #4dd0e1;
    }

    .copiado {
        position: absolute;
        height: 100%;
        width: 100%;
        background: rgb(0,0,0,0.8);
        text-align: center;
        top: 0;
        line-height: 300px;
        color: white;
        font-size: 20px;
        display:none;
    }

    .hidden {
        display: none !important;
    }
</style>

<script>
    $(document).ready(function() {
        function readURL(input, doom) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();                
                reader.onload = function (e) {                   
                    $('.'+doom).attr('src', e.target.result).show();
                }
                reader.readAsDataURL(input.files[0]);
                //$('.'+doom).val(input.files[0].name);              
            }
        }

        $(".imgFondo").change(function(){
            readURL(this, 'previoFondo');
        });

        $(".btnShow").on('click', function() {
            if ($('.contentURL').hasClass('hidden'))
                $('.contentURL').removeClass('hidden');
            else
                $('.contentURL').addClass('hidden');
        });
    });
    var btn = document.querySelector(".code-javascript");
    if(btn) {
        btn.onclick = function() {
            var copiado = document.querySelector('.copiado');
            copiado.style.display = 'block';
            var result = copyToClipboard(document.querySelector('#code_copy').textContent);
            console.log("copied?", result);
            setTimeout(() => {
                copiado.style.display = 'none';
            }, 3800);
        };
    }    
</script>

@push('scripts')
<script src="/public/js/profile.js?t={{now()->timestamp}}"></script>
@endpush
@endsection