<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer">
    © <?=date('Y')?> Admin Linkeados
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
<!--Custom JavaScript -->
<script src="/public/js/custom.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<script src="/public/js/dashboard4.js"></script>
<script src="/public/js/jo.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
