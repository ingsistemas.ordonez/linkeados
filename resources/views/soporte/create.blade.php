@extends('layout') 
@section('content') 

<div class="container-fluid m-t-20">
    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-lg-12">
            <form action="{{route('soporte.createprocess')}}" id="form-soporte" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <div class="card card-outline-info">              
                    <div class="card-body">
                        <div class="form-body">
                            <h3 class="box-title">CREAR REPORTE</h3>
                            <hr class="m-t-0 m-b-40">                            
                            <div class="row">                            
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Nombre</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="nombre" required maxlength="100">
                                        </div>
                                    </div>    
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Tipo de error</label>
                                        <div class="col-md-8 input-group">
                                            <select class="form-control custom-select" name="tipo_error" required>
                                                <option value=""></option>
                                                @foreach($tipoError as $te)
                                                <option value="{{$te->id}}">{{$te->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Comentarios</label>
                                        <div class="col-md-8 input-group">
                                            <textarea class="form-control" name="comentario" rows="3" required></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4"></label>
                                        <div class="col-md-8">
                                            <div class="row content-img"></div>
                                        </div>
                                        <label class="control-label text-right col-md-4 text-uppercase">Img de error</label>
                                        <div class="col-md-8 input-group input-uploads-files">
                                            <input type="file" id="adjunto_soporte" class="d-none" accept="image/x-png,image/gif,image/jpeg">
                                            <button type="button" class="btn btn-light btn-upload">Seleccionar archivo
                                                <i class="fa fa-camera float-right"></i>
                                            </button>
                                            <br/>
                                            <span style="color : red;" class="alert-upload d-none">Seleccione un archivo</span>
                                        </div>
                                    </div> 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Teléfono</label>
                                        <div class="col-md-8 input-group">
                                            <input type="tel" class="form-control" name="telefono" value="" required maxlength="45">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Correo</label>
                                        <div class="col-md-8 input-group">
                                            <input type="email" class="form-control" name="correo" value="" required maxlength="45">
                                        </div>
                                    </div>
                                    @if(Auth::user()->cc_id != 2)
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Sitio web</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="sitio_web" value="" required maxlength="255">
                                        </div>
                                    </div>
                                    @endif

                                    <div class="form-group row justify-content-center">
                                        <div class="spinner-border text-primary d-none loading-icon" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6 col-md-logo-soporte">
                                    <img src="/assets/images/logo-soporte.png" alt="logosoporte" class="dark-logo center img-soporte" />
                                </div>

                            </div>                      
                        </div>
                    </div>                
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            @method('PUT')                      
                            <button type="submit" class="btn-md btn-primary float-right  m-r-10">Enviar reporte</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .box-title{
        font-weight: bold;
    }

    .col-md-logo-soporte{
        background: rgb(235,235,235);
    }

    .btn-upload{
        background-color: white;
        border: 1px solid gray;
        border-radius: 5px;
        width: 100%;
    }

    .img-soporte{
        max-height: 100%;
        max-width: 100%;
        width: auto;
        height: auto;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }
    
    .form-control{
      border-right:  1px solid #ced4da !important;
    }

</style>

@push('scripts')
<script src="/public/js/soporte.js?t={{now()->timestamp}}"></script>
@endpush
@endsection