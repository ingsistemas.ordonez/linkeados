@extends('layout') 
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Datos Bancarios</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('pagos.index')}}">Pagos</a></li>
            <li class="breadcrumb-item active">Dato Bancarios</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">            
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Datos</h4>
                </div>
                @if($datos)
                <div class="card-body">                    
                    <div class="form-body">
                        <h3 class="box-title">Información</h3>
                        <hr class="m-t-0 m-b-40">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nombre:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->nombre}} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Banco:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->banco_nombre}} </p>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Cuenta:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->cuenta}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Tipo Cuenta:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->tipo}} </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Correo:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->correo}} </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Teléfono:</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static"> {{$datos->telefono}} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>
                @else
                <h2 class="text-center">No existe datos bancarios de este usuario</h2>
                @endif
            </div>          
        </div>
    </div>
</div>
@endsection
