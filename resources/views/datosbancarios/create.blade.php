@extends('layout') 
@section('content')
<div class="container-fluid m-t-20">
    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-lg-12">
            <form action="{{route('datosbancarios.createprocess')}}" id="form-datos-bancarios" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <div class="card card-outline-info">              
                    <div class="card-body">
                        <div class="form-body">
                            <h3 class="box-title">DATOS BANCARIOS</h3>
                            <hr class="m-t-0 m-b-40">                            
                            <div class="row">                            
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="nombre" value="{{($datosBancariosUser) ? $datosBancariosUser->nombre : ''}}" required maxlength="100">                                           
                                        </div>
                                    </div>                                      

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Banco</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="banco_nombre" value="{{($datosBancariosUser) ? $datosBancariosUser->banco_nombre : ''}}" required>                                           
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Cuenta</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="cuenta" value="{{($datosBancariosUser) ? $datosBancariosUser->cuenta : ''}}" required maxlength="45" placeholder="#### #### #### ####">                                          
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Tipo cuenta</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="tipo_cuenta" required>
                                                <option value=""></option>
                                                <option value="AHORROS" {{($datosBancariosUser && $datosBancariosUser->tipo == 'AHORROS') ? 'selected' : ''}}>AHORROS</option>
                                                <option value="CORRIENTE" {{($datosBancariosUser && $datosBancariosUser->tipo == 'CORRIENTE') ? 'selected' : ''}}>CORRIENTE</option>
                                            </select>
                                        </div>
                                    </div>

                                    <!--
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Carta cuenta</label>
                                        <div class="col-md-8 input-group">
                                            <input type="file" name="contancia_activa" id="constancia_activa" class="d-none">
                                            <button type="button" class="btn btn-light btn-upload">
                                                <span>{{($datosBancariosUser) ? $datosBancariosUser->constancia_activa : 'Constancia cuenta activa'}}</span>
                                                <i class="fa fa-cloud-upload float-right"></i>
                                            </button>
                                            @if($datosBancariosUser)
                                            <a class="url-prev-constancia" href="/public/uploads/constancias/{{$datosBancariosUser->constancia_activa_url}}" target="_blank">{{$datosBancariosUser->constancia_activa}}</a>
                                            @endif
                                            <br/>
                                            <span style="color : red;" class="alert-upload d-none">Seleccione un archivo</span>
                                        </div>
                                    </div> 
                                    -->

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Correo</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control" name="correo" value="{{($datosBancariosUser) ? $datosBancariosUser->correo : ''}}" maxlength="100">                                            
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <!--
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Fecha de pago</label>
                                        <div class="col-md-8">
                                            <input type="date" class="form-control" value="{{($datosBancariosUser) ? Carbon\Carbon::parse($datosBancariosUser->fecha_pago)->format('yy-m-d') : ''}}" name="fecha_pago">
                                        </div>
                                    </div>
                                    -->

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Teléfono</label>
                                        <div class="col-md-8">
                                            <input type="tel" class="form-control" name="telefono" maxlength="20" placeholder="## ## ## ##" value="{{($datosBancariosUser) ? $datosBancariosUser->telefono : ''}}">                                           
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4 text-uppercase">Dirección</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="direccion" maxlength="100" value="{{($datosBancariosUser) ? $datosBancariosUser->direccion : ''}}">                                           
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <div class="spinner-border text-primary d-none loading-icon" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </div>

                            </div>                      
                        </div>
                    </div>                
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            @method('PUT')                      
                            <button type="submit" class="btn-md btn-primary float-right m-r-10">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .box-title{
        font-weight: bold;
    }

    .btn-upload{
        background-color: white;
        border: 1px solid gray;
        border-radius: 5px;
        width: 100%;
    }

</style>
@push('scripts')
<script src="/public/js/datosbancarios.js?t={{now()->timestamp}}"></script>
@endpush
@endsection
