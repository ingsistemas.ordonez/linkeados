@extends('layout') 
@section('content') 
<div class="container-fluid p-t-20">
    <div class="row">
        <div class="col-12">            
            <div>
                <div class="card-body">
                    <h2 class="card-title">Productos</h2>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('productos.create') }}" class="btn-md btn-primary float-left m-r-5 btn-especial disabled">
                                Crear Producto 
                            </a>
                            <a href="" class="btn-md btn-primary float-left btn-especial">
                                Carga Masiva 
                            </a>
                        </div>
                    </div>
                    <br/>
                    <div class="row"> 
                        <div class="col-lg-12">
                            <form action="{{route('cargamasiva.proccess')}}" id="form-carga-masiva" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                <input type="hidden" value="ADMIN" id="type">
                                <div class="card card-outline-info">
                                    <div class="card-body">
                                        <div class="form-body">
                                            <h3 class="box-title">CARGA MASIVA</h3>
                                            <div class="alert alert-danger error-excel"></div>
                                            <div class="row">
                                                <div class="col-md-4 text-right">
                                                    <label>ARCHIVO</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="file" id="file-xml" class="d-none" accept=".xlsx, .xls, .csv" required="">
                                                    <button type="button" class="btn btn-light btn-upload">Seleccionar archivo</button>
                                                    <br/>
                                                    <span style="color : red;" class="alert-upload d-none">Seleccione un archivo</span>
                                                    <span style="color : green;" class="ok-upload"></span>
                                                    <span style="color : #c3c3c3;" class="file-upload"></span>                
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="spinner-border text-primary d-none loading-icon" role="status">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p>Descarga nuestra plantilla para que puedas cargar en tu cuenta los productos masivamente</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <a target="_blank" href="{{route('cargamasiva.template')}}" type="button" class="btn-md float-right m-r-10 btn-orange btn-download-template">ARCHIVO DEMO</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @csrf
                                            @method('PUT')                      
                                            <button type="button" class="btn-md btn-primary float-right m-r-10 btn-action-upload">CARGAR</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .btn-upload{
        background-color: white;
        border: 1px solid gray;
        border-radius: 5px;
        width: 80%;
    }

    .btn-orange, .btn-orange:hover{
        background: rgb(235,205,19);
        border: none;
        color: #fff !important;
    }
</style>

@push('scripts')
<script src="./public/js/cargamasiva.js?t={{now()->timestamp}}"></script>
@endpush
@endsection