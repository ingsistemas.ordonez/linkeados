@extends('layout') 
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Crear Producto</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('productos.index')}}">Productos</a></li>
            <li class="breadcrumb-item active">Nuevo Producto</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="container-fluid">   

    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-lg-12">
            <form action="{{ route('productos.store') }}" method="POST" class="form-horizontal form-producto" enctype="multipart/form-data">
                <div class="card card-outline-info">
                    <div class="card-body">
                        <div class="form-body">
                            <h6 class="box-title">Ingrese los siguiente datos</h6>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Nombre</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="nombre_p" value="{{ old('nombre_p') }}" required>
                                            
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Categoria</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="cp_id" required>
                                                @foreach($cc as $item)
                                                <option value="{{$item->id}}" @if( $item->id == old('cp_id') ) {!! _('selected') !!} @endif >{{$item->nombre_cp}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> 
                                    <!--
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Sitio Web</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="web" value="{{ old('web') }}" required> 
                                        </div>
                                    </div>
                                    -->
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Link Producto</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="url_web" value="{{ old('url_web') }}" required>
                                            
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">SKU</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="sku" value="{{ old('sku') }}" required>                                           
                                        </div>
                                    </div>      
                                    <div class="form-group row m-b-10">
                                        <label class="control-label text-right col-md-4">Descripción</label>
                                        <div class="col-md-8">
                                            <textarea rows="5" class="form-control" name="descripcion" style="height: 80px;">{{ old('descripcion') }}</textarea>
                                        </div>
                                    </div>                                                 
                                </div>

                                <div class="col-md-6">                                                 
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Porcentaje Afiliado</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="porcentaje_a" value="{{ old('porcentaje_a') }}" required>
                                            
                                        </div>
                                    </div>                                                     

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Img.Perfil</label>
                                        <div class="col-md-8">
                                            <div class="content-fotos row">
                                                <div class="col-3" style="display:block">
                                                    <div class="file-field addFile">
                                                        <div class="btn btn-photo btn-sm">
                                                            <i class="fa-camera fa"></i>
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div>                 
                                        </div>
                                    </div>      
                                    
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Conversión</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="conversion" required>
                                                <option value="venta">VENTA</option>
                                                <option value="pagina llegada">PAGINA LLEGADA</option>
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Valor</label>
                                        <div class="col-md-8 ">
                                            <input type="text" class="form-control" name="valor" value="{{ old('valor') }}" required>
                                            
                                        </div>
                                    </div> 
                                </div>
                            </div>   
                        </div>                        
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            <a href="{{route('productos.index')}}" class="btn-md btn-inverse float-right">Cancelar</a>
                            <button type="button" class="btn-md btn-primary float-right m-r-10 btn-form">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {       
        var cont = 1;
        $('.addFile').on('click', ()=>{
            var htmlimg = '<div class="col-3 block_'+cont+'"><i class="fa fa-trash" onclick="delImage('+cont+')"></i><img class="previoFondo m-b-5" style="width: 100%;"><input type="file" class="form-control imgFondo" name="url_imagen[]" data-cont="'+cont+'" onChange="readURL(this, '+cont+')"></div>';

            $( htmlimg ).prependTo( ".content-fotos" );
            $( ".block_"+cont+" [type='file']" ).trigger( "click" );
            cont++;
        });        
    });

    function readURL(input, doom) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();                
            reader.onload = function (e) {                   
                $('.block_'+doom+' .previoFondo').attr('src', e.target.result).show();
                $('.block_'+doom).show();
            }
            reader.readAsDataURL(input.files[0]);
            //$('.'+doom).val(input.files[0].name);             
        }
    }

    function delImage(doom) {
        $('.block_'+doom).remove();
    }

    var btnSubmit = document.querySelector(".btn-form");
    btnSubmit.onclick = () => {
        var images = document.querySelectorAll(".previoFondo");
        if(images.length == 0){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Se requiere una foto del producto.'
            });
            return false;
        }
        var form = document.querySelector(".form-producto");
        form.submit();
    };
</script>

<style>
    .content-fotos .addFile i {
        position: relative !important;
        top: 0 !important;
        right: 0 !important;
        color: #432874 !important;
    }
    .content-fotos .col-3 {
        margin: 0;
        padding: 0;
        margin-right: 5px;
        overflow: hidden;
        display: none;
    }
    .content-fotos i {
        position: absolute;
        right: 5px;
        top: 5px;
        color: white;
    }
    input[type="file"] {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 0;
        margin: 0;
        cursor: pointer;
        filter: alpha(opacity=0);
        opacity: 0;
        display:none;
    }
</style>

@endsection
