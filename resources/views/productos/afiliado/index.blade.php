@extends('layout') 
@section('content')
<div class="container-fluid p-t-20">
    <div class="row">
        <div class="col-12">            
            <div class="">
                <div class="card-body">
                    <h2 class="card-title">Productos</h2>
                    <h6 class="card-subtitle">Lista de productos</h6>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">                          
                            <a href="{{ route('links.index') }}" class="btn-md btn-info float-left btn-especial">
                                Mis Links 
                            </a>
                            <a href="#" class="btn-md btn-outline-primary float-right btn-especial btn-search">
                               Organizar 
                               <i class="fa fa-angle-up"></i>
                            </a>
                            <a href="#" class="btn-md btn-primary float-right btn-especial btn-search-open" style="display:none">
                               Organizar 
                               <i class="fa fa-angle-down"></i>
                            </a>
                            <div class="search_float">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="{{ route('productos.index') }}" method="GET" class="form-horizontal" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h6 class="box-title">Filtrar por:</h6>
                                                <hr class="m-t-0 m-b-5">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-12">Ordenar productos</label>
                                                            <div class="col-12">
                                                                <select class="form-control custom-select" name="orden_p">     
                                                                    <option value="DESC">Descendiente</option>
                                                                    <option value="ASC">Ascendiente</option>
                                                                </select>
                                                            </div>
                                                        </div>             
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-md-12">Categoria</label>
                                                            <div class="col-md-12">
                                                                <select class="form-control custom-select" name="cp_id">
                                                                    <option value="">Seleccione</option>
                                                                    @foreach($cc as $item)
                                                                    <option value="{{$item->id}}" @if( $item->id == old('cp_id') ) {!! _('selected') !!} @endif >{{$item->nombre_cp}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>             
                                                    </div>

                                                    <div class="col-12"> 
                                                        <div class="form-group row">
                                                            <label class="control-label text-right col-12">Valor</label>
                                                            <div class="col-12 input-group"> 
                                                                <div class="price-slider"><span>de
                                                                    $<input type="number" value="0" min="0" max="2500" name="valor_i"/> a
                                                                    $<input type="number" value="500" min="0" max="2500" name="valor_f"/></span>

                                                                    <input type="hidden" id="valor" name="valor" value="false"/></span>

                                                                    <input value="0" min="0" max="2500" step="10" type="range"/>
                                                                    <input value="500" min="0" max="2500" step="10" type="range"/>

                                                                    <svg width="100%" height="24">
                                                                        <line x1="4" y1="0" x2="300" y2="0" stroke="#212121" stroke-width="12" stroke-dasharray="1 28"></line>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>  
                                                
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            @csrf   
                                                            <button type="submit" class="btn-md btn-primary m-r-10 mx-auto" style="display: block;">Buscar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                        
                                        </div>
                                    </div>                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-12">
                            <div class="card p-2">
                                <div class="row">
                                    <div class="col-1"></div>
                                    <div class="col-3 border-right font-size-11">Nombre producto</div>
                                    <div class="col-2 border-right font-size-11">Categor&iacute;a</div>
                                    <div class="col-1 border-right font-size-11">Precio</div>
                                    <div class="col-1 border-right font-size-11">Comisión</div>
                                    <div class="col-2 border-right font-size-11">Link producto</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($productos as $item)
                        <div class="col-12">
                            <div class="card p-2">
                                <div class="row">
                                    <div class="col-1 content-img-p center-conent-v">
                                        {!! $item->imagenlist !!}
                                    </div> 
                                    <div class="col-3 border-right center-conent-v">
                                        <h4 style="font-weight: bold;">
                                            {{$item->nombre_p}}                                       
                                        </h4> 
                                    </div>
                                    <div class="col-2 border-right center-conent-v">
                                        {{$item->categoriap->nombre_cp}}
                                    </div>
                                    <div class="col-1 border-right center-conent-v">
                                        ${{$item->valor}}
                                    </div>
                                    <div class="col-1 border-right center-conent-v text-calculovalor">
                                        {{$item->calculovalor}}%
                                    </div> 
                                    <div class="col-2 border-right center-conent-v">
                                        {!! $item->yalinkeado !!}
                                    </div>   
                                    <div class="col-2 center-conent-v">
                                        <div class="btn-group float-left" role="group">  
                                            <a href="{{ route('productos.show', $item) }}" class="btn btn-primary btn-especial" style="max-width: 100px;margin: auto;">
                                                VER    
                                            </a>                                               
                                        </div>
                                    </div>                           
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>  
                    @if($productos->count() == 0)
                    <div class="row m-t-40">
                        <div class="col-12">
                            <h2 class="text-center">No tiene productos ingresados</h2>
                        </div>
                    </div>        
                    @endif
                    @if($productos->hasPages())
                    <div class="row">
                        <div class="col-12">
                            {{ $productos->links() }}
                        </div>
                    </div>          
                    @endif
                </div>
            </div>            
        </div>
    </div>
</div>

<style>
    .only-btn {
        padding: 3px;
    }
    .img-circle {
        height: 50px;
        width: 50px;
        margin: 0 auto;
        margin-top: 5px !important;
        display: block;
        float: none !important;
    }

    .btn-secondary:hover {
        color: #E7D10A !important;
        background-color: transparent;
        border-color: none;
        box-shadow: none !important;
    }

    .text-limit {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2; /* number of lines to show */
        -webkit-box-orient: vertical;
        width: 75%;
    }
    .box-link {
        font-size: 12px;
    }
    .card {
        margin-bottom: 15px;
    }
    .border-right {
        border-right: 1px solid #ccc;
        text-align: center;
    }
    .box-link {
        padding: 5px 12px;
    }

    .center-conent-v{
        display: flex;
        justify-content: center;
        flex-direction: column;
    }

    .text-calculovalor{
        font-weight: bold;
        color: #432874;
        font-size: 20px;
    }

    .font-size-11{
        font-size: 11px;
    }
    
    .content-img-p > img{
        margin-top: 0px !important; 
    }

</style>
@endsection    