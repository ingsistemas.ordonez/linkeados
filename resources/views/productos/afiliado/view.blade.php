@extends('layout') 
@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Producto - {{$producto->nombre_p}}</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('productos.index')}}">Productos</a></li>
            <li class="breadcrumb-item">Ver</li>
            <li class="breadcrumb-item active">{{$producto->id}}</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="container-fluid p-t-20">
    <div class="row">
        <div class="col-12">  
            <div class="card">
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-6">
                            <div class="owl-carousel owl-theme">
                                @foreach($producto->productofotos as $item)
                                    @if($item->tipo == 'IN')
                                    <img class="d-block img-fluid owl-lazy" data-src="{{ url('storage/app/public/'.$item->url_imagen) }}">
                                    @else
                                    <img class="d-block img-fluid owl-lazy" data-src="{{ $item->url_imagen }}">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-md-12 d-xs-none">
                                    <div class="mt-5"><br/></div>
                                </div>
                            </div>
                            <h1 class="text-left title1">{{$producto->nombre_p}}</h1>
                            <h3 class="float-left title2 m-b-10">{{$producto->categoriap->nombre_cp}}</h3>
                            <br/>
                            
                            <div class="m-t-10" style="width: 190px;">
                                {!! $producto->yalinkeado !!}
                            </div>

                            <div class="form-group m-t-20">
                                {{$producto->descripcion}}
                            </div>

                            <div class="form-group m-t-20">
                                SKU: {{$producto->sku}}
                            </div>

                            <div class="row">
                                <div class="col-6 b-r">
                                    <h4 class="title3">VALOR ESTIMADO</h4>
                                    <h2 class="title4">${{$producto->valor}}</h2>
                                </div>
                                <div class="col-6 text-center">
                                    <h4 class="title3">PORCENTAJE</h4>
                                    <h2 class="title4">{{$producto->calculovalor}}%</h2>
                                </div>
                            </div>
                            <br/>
                            <div class="form-group m-t-20">
                                <form method="POST" action="{{ route('productos.linkear', $producto) }}">
                                    @csrf
                                    <button type="submit" class="btn-md btn-primary" style="font-weight: bold;">
                                        AGREGAR A MIS LINKS
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            items:1,
            lazyLoad:true,
            loop:true,
            nav:false
        });
    });
</script>

<style>
.title1 {
    font-size: 36px;
    color: #432874;
    font-weight: bold;
    text-transform: uppercase;
}
.title2, .title3 {
    text-transform: uppercase;
    font-size: 15px;
}
.title3 {
    color: #e4ce0b;
}
.title4 {
    color: #7a7a7a;
    font-size: 36px;
}
.b-r {
    border-right: 1px solid #ccc;
}
</style>

@endsection    