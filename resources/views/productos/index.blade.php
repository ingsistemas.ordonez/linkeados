@extends('layout') 
@section('content')
<div class="container-fluid p-t-20">
    <div class="row">
        <div class="col-12">            
            <div class="">
                <div class="card-body">
                    <h2 class="card-title">Productos</h2>
                    <h6 class="card-subtitle">Lista de productos</h6>
                    <br/>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('productos.create') }}" class="btn-md btn-primary float-left m-r-5 btn-especial">
                                Crear Producto 
                            </a>
                            <a href="{{ route('cargamasiva.index') }}" class="btn-md btn-primary float-left btn-especial">
                                Carga Masiva 
                            </a>
                        </div>
                    </div>
                    <br/>
                    <div class="row content-list-products">
                        @foreach($productos as $item)
                        <div class="col-md-4">
                            <div class="card p-2" style="min-height: auto;">
                                <div class="row">
                                    <div class="col-4">
                                        {!! $item->imagenlist !!}
                                    </div> 
                                    <div class="col-8">
                                        <h4 style="font-weight: bold; clear: both;">
                                            <div class="float-left text-limit">{{$item->nombre_p}}</div>
                                            <div class="btn-group float-right" role="group" aria-label="But with nested dropdown">     
                                                <form method="POST" action="{{ route('productos.duplicate', $item) }}">
                                                    @csrf
                                                    <button type="submit" class="btn btn-secondary font-18 only-btn">
                                                        <i class="fa fa-copy m-r-5"></i>
                                                    </button>
                                                </form>

                                                <a href="{{ route('productos.edit', $item) }}" class="btn btn-secondary font-18 only-btn"><i class="fa fa-pencil"></i></a>    

                                                <form method="POST" action="{{ route('productos.destroy', $item) }}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-secondary font-18 only-btn">
                                                        <i class="fa fa-trash m-r-5"></i>
                                                    </button>
                                                </form>                                           
                                            </div>
                                        </h4> 
                                        <div style="clear: both;"></div>
                                        <hr/>
                                        <div class="m-t-10">
                                            {!! $item->yalinkeado !!}
                                            <!--
                                            <div style="display:block">
                                                <i class="fa fa-link float-left"></i> 
                                                <div class="float-right">{{$item->codigo}}</div>
                                                <div style="clear:both"></div>
                                            </div>
                                            -->
                                        </div>
                                    </div>  
                                    <div class="col-md-12">
                                        <h6 class="m-t-20">
                                            <div class="row">
                                                <div class="col-9" style="padding-left: 30px;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">
                                                    {{$item->username->name}}
                                                </div>
                                                <div class="col-3">
                                                    ID:{{$item->id}}
                                                </div>
                                            </div>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @if(count($productos) == 0)
                        <h2 class="mx-auto">No se encontraron productos</h2>
                        @endif
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<style>
    .only-btn {
        padding: 3px;
    }
    .img-circle {
        height: 100px;
        width: 100px;
        margin: 0 auto;
        margin-top: 20px !important;
        margin-left: 4px;
    }

    .btn-secondary:hover {
        color: #E7D10A !important;
        background-color: transparent;
        border-color: none;
        box-shadow: none !important;
    }

    .text-limit {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 1; /* number of lines to show */
        -webkit-box-orient: vertical;
        width: 60%;
    }

    .card-no-border .card {
        min-height: 215px;
    }
</style>
@endsection    