@extends('layout') 
@section('content')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Editar Pagos</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('pagos.index')}}">Pagos</a></li>
            <li class="breadcrumb-item active">{{$pago->id}}</li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="container-fluid">   

    @if($errors->any())
    <div class="card border-0 shadow">
        <div class="card-body">
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                - {{ $error }} <br/>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="row"> 
        <div class="col-lg-12">
            <form action="{{ route('pagos.update', $pago) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card card-outline-info">
                    <div class="card-body">
                        <div class="form-body">
                            <h6 class="box-title">Ingrese los siguiente datos</h6>
                            <hr class="m-t-0 m-b-40">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Comisión Afiliado</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="comision_afiliado" value="{{ old('comision_afiliado', $pago->comision_afiliado) }}" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Comisión Admin</label>
                                        <div class="col-md-8 input-group">
                                            <input type="text" class="form-control" name="comision_admin" value="{{ old('comision_admin', $pago->comision_admin) }}" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">
                                                    <i class="fa fa-pencil"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>                     
                                </div>
                                <div class="col-md-6">                                   
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Estado Comerciante</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="estado_comerciante">    
                                                @foreach($pago->estados_pagos  as $index => $estado)
                                                    <option value="{{$index}}"
                                                    @if($pago->estado_comerciante == $index)
                                                    selected
                                                    @endif>{{$estado}}</option>
                                                @endforeach        
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-4">Estado Afiliado</label>
                                        <div class="col-md-8">
                                            <select class="form-control custom-select" name="estado_afiliado">
                                                @foreach($pago->estados_pagos  as $index => $estado)
                                                    <option value="{{$index}}"
                                                    @if($pago->estado_afiliado == $index)
                                                    selected
                                                    @endif>{{$estado}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>   
                        </div>                        
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">                            
                            <a href="{{route('pagos.index')}}" class="btn-md btn-inverse float-right">Cancelar</a>
                            <button type="submit" class="btn-md btn-primary float-right m-r-10">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>          
        </div>
    </div>
</div>

@endsection
