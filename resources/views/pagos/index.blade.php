@extends('layout') 
@section('content')
<div class="container-fluid p-t-20">
    <div class="row">
        <div class="col-12">            
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Pagos Generales</h2>
                    <h6 class="card-subtitle">Lista de pagos</h6>
                    <div class="row">
                        <div class="col-md-12">
                           
                            @if(Auth::user()->cc_id == 1)
                            <a href="javascript:void(0);" class="btn-md btn-primary float-left m-r-5 btn-especial btn-liquidarpago">
                               Liquidar Pagos 
                            </a>
                            @endif

                            @if(Auth::user()->cc_id == 2)
                            <a href="javascript:void(0);" class="btn-md btn-info float-left m-r-5 btn-especial btn-solicitarpago">
                               Solicitar Pagos 
                            </a>
                            @endif
                            @if(Auth::user()->cc_id == 3)
                            <a href="?estado=EAC" class="btn-md btn-primary float-left m-r-5 btn-especial">
                                Filtrar Liquidaciones de Pagos
                            </a>
                            <a href="?estado=EA" class="btn-md btn-info float-left m-r-5 btn-especial">
                               Filtrar Solicitudes de Pagos
                            </a>
                            @endif

                            @if(empty($_GET['estado']))
                            <a href="?estado=C" class="btn-md btn-dark float-left btn-especial">
                               Historial Pagos 
                            </a>
                            @else
                            <a href="{{route('pagos.index')}}" class="btn-md btn-dark float-left btn-especial">
                               Pagos en Espera
                            </a>
                            @endif

                            @if(Auth::user()->cc_id == 2)
                            <a href="{{ route('datosbancarios.create') }}" class="btn btn-outline-primary btn-md float-right btn-especial">
                               Datos Bancarios 
                            </a>
                            @endif
                            @if(Auth::user()->cc_id == 1)
                            <a href="{{ route('configuracion.edit', 1) }}" class="btn btn-outline-primary btn-md float-right btn-especial">
                               Datos Bancarios 
                            </a>
                            @endif
                        </div>
                    </div>

                    <div class="table-responsive m-t-10">
                        @if(Auth::user()->cc_id == 3)
                        <table id="myTable" class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable">
                            <thead>
                                <tr style="background: #ebebeb;">
                                    <th>ID</th>
                                    <th>Usuario</th>
                                    <th>Tipo</th>
                                    <th>V.C.A</th>
                                    <th>V.C.ADMIN</th>
                                    <th>Producto</th>
                                    <th>Fecha</th>
                                    <th>E.Afiliado</th>
                                    <th>E.Comerciante</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>                             
                                @foreach($pagos as $pago)
                                <tr>
                                    <td>{{ $pago->id }}</td>
                                    <td>{{ $pago->users->name }}</td>                                    
                                    <td>{{ $pago->tipo }}</td>                                    
                                    <td>${{ $pago->comision_afiliado }}</td>                                    
                                    <td>${{ $pago->comision_admin }}</td> 
                                    <td>{{ $pago->producto_nombre }}</td>                                    
                                    <td>{{ $pago->format_fecha }}</td>                                    
                                    <td>{!! $pago->estado_afiliado_dinamico !!}</td>                                    
                                    <td>{!! $pago->estado_comercio_dinamico !!}</td>                                    
                                    <td style="padding:0">
                                        <div class="btn-group" role="group">              
                                            <a href="{{route('pagos.edit', $pago)}}" class="btn btn-secondary font-18 only-btn"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('datosbancarios.show', $pago->users_id)}}" class="btn btn-secondary font-18 only-btn"><i class="fa fa-university"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @elseif(Auth::user()->cc_id == 2)
                        <form action="{{ route('pagos.spa') }}" method="POST" class="form-horizontal form-spa" enctype="multipart/form-data">
                            @csrf  
                            <table id="myTable" class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable">
                                <thead>
                                    <tr style="background: #ebebeb;">
                                        <th>ID</th>
                                        <th>Comisión</th>
                                        <th>Producto</th>
                                        <th>Comercio</th>
                                        <th>Fecha</th>
                                        <th>Estado A</th> 
                                    </tr>
                                </thead>                                                        
                                <tbody>                             
                                    @foreach($pagos as $pago)
                                    <tr>
                                        <td>
                                            <div class="float-left">{{ $pago->id }}</div>
                                            @if($pago->estado_afiliado == 'E')
                                            <input type="checkbox" class="form-check-input" name="estados[]" value="{{ $pago->id }}" data-valor="{{ $pago->comision_afiliado }}">
                                            @endif
                                        </td>                           
                                        <td>${{ $pago->comision_afiliado }}</td>
                                        <td>{{ $pago->producto_nombre }}</td>                                        
                                        <td>{{ $pago->producto->username->name }}</td> 
                                        <td>{{ $pago->format_fecha }}</td>                                  
                                        <td>{!! $pago->estado_afiliado_dinamico !!}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th style="text-align:right">Total:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </form>                       
                        @elseif(Auth::user()->cc_id == 1)
                        <form action="{{ route('pagos.spl') }}" method="POST" class="form-horizontal form-spl" enctype="multipart/form-data">
                            @csrf 
                            <table id="myTable" class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable">
                                <thead>
                                    <tr style="background: #ebebeb;">
                                        <th>ID</th>
                                        <th>V.C</th>
                                        <th>Producto</th>
                                        <th>Fecha</th>
                                        <th>Estado C</th>
                                    </tr>
                                </thead>
                                <tbody>                             
                                    @foreach($pagos as $pago)
                                    <tr>
                                        <td>
                                            <div class="float-left">{{ $pago->id }}</div>
                                            @if($pago->estado_comerciante == 'E')
                                            <input type="checkbox" class="form-check-input" name="estados[]" value="{{ $pago->id }}">
                                            @endif
                                        </td> 
                                        <td>{{ $pago->producto_nombre }}</td>                                 
                                        <td>${{ $pago->sumaTotal() }}</td> 
                                        <td>{{ $pago->format_fecha }}</td>                                  
                                        <td>{!! $pago->estado_comercio_dinamico !!}</td>    
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2" style="text-align:right">Total:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </form>
                        @endif
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<script>
    @if(Auth::user()->cc_id == 1)
    var btnSubmit = document.querySelector(".btn-liquidarpago");
    btnSubmit.onclick = () => {
        

        let checked = document.querySelectorAll('.form-check-input:checked');
        if (checked.length === 0) {
            // there are no checked checkboxes
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Debe seleccionar mínimo un registro!'
            })
            return false;
        } 

        Swal.fire({
            title: "¿Esta seguro de liquidar los pagos?",
            text: "Se enviara las solicitudes de liquidación al administrador!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, enviarlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                var form = document.querySelector(".form-spl");
                form.submit();
            }
        });
    };
    @endif
    @if(Auth::user()->cc_id == 2)
    var btnSubmit = document.querySelector(".btn-solicitarpago");
    btnSubmit.onclick = () => {

        var pagos = 0;
        let checked = document.querySelectorAll('.form-check-input:checked');
        checked.forEach(function(el) {
            pagos += parseFloat(el.getAttribute("data-valor"));
        });

        if(pagos <= 10){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'La solicitud de pago debe ser mayor a los $10.'
            });

            return false;
        }

        if (checked.length === 0) {
            // there are no checked checkboxes
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Debe seleccionar mínimo un registro!'
            })
            return false;
        } 

        Swal.fire({
            title: "¿Esta seguro de solicitar los pagos?",
            text: "Se enviara las solicitudes de pago al administrador!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, enviarlo!'
        }).then((result) => {
            if (result.isConfirmed) {
                var form = document.querySelector(".form-spa");
                form.submit();
            }
        });
    };
    @endif
</script>

<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"           
            },
            "order": [
                [0, 'desc']
            ],
            "displayLength": 25,
            @if(Auth::user()->cc_id == 3)
            "columnDefs": [
                { "width": "15%", "targets": 1 },
                { "width": "12%", "targets": 3 },
                { "width": "5%", "targets": 5 }
            ],            
            "footerCallback": function ( row, data, start, end, display ) {
                //console.log(data)
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                total2 = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                pageTotal2 = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        console.log(intVal(a) +'+'+ intVal(b))
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                pageTotal = parseFloat(pageTotal).toFixed(2);
                total = parseFloat(total).toFixed(2);
                $( api.column( 3 ).footer() ).html(
                    '$'+ pageTotal +'<br/>( $'+ total +' Total)'
                );

                pageTotal2 = parseFloat(pageTotal2).toFixed(2);
                total2 = parseFloat(total2).toFixed(2);
                $( api.column( 4 ).footer() ).html(
                    '$'+pageTotal2 +'<br/>( $'+ total2 +' Total)'
                );
            }
            @endif
            @if(Auth::user()->cc_id == 2)
            "footerCallback": function ( row, data, start, end, display ) {
                //console.log(data)
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                
                // Total over this page
                pageTotal = api
                    .column( 1, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
            
                // Update footer
                pageTotal = parseFloat(pageTotal).toFixed(2);
                total = parseFloat(total).toFixed(2);
                $( api.column( 1 ).footer() ).html(
                    '$'+ pageTotal +'<br/>( $'+ total +' Total)'
                );
            }
            @endif
            @if(Auth::user()->cc_id == 1)
            "footerCallback": function ( row, data, start, end, display ) {
                //console.log(data)
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                
                // Total over this page
                pageTotal = api
                    .column( 2, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
            
                // Update footer
                pageTotal = parseFloat(pageTotal).toFixed(2);
                total = parseFloat(total).toFixed(2);
                $( api.column( 2 ).footer() ).html(
                    '$'+ pageTotal +'<br/>( $'+ total +' Total)'
                );
            }
            @endif
        });       
    });  
</script>

<style>
input[type=checkbox] {
    position: relative !important;
    left: 0 !important;
    opacity: 1 !important;
    width: 15px;
    float: right;
    height: 15px;
}
</style>
@endsection    