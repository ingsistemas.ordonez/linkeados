<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;   

class Links extends Model
{ 
    protected $table = 'links';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo','productos_id','users_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    // 
    /*protected $guarded = ['id'];

    protected $fillable = [
        'codigo', 'productos_id', 'users_id'
    ];*/

    public function setCodigoAttribute() 
    {
        $length = 10;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $this->attributes['codigo'] = $randomString;
    }  

    public function producto()
    {
        return $this->belongsTo(Productos::class, 'productos_id');
    }

    public function getLinkeadoAttribute()
    {
        return $this->producto->url_web.'?codigo='.$this->producto->codigo.'-'.$this->codigo;
    }

    public function getTimelinkAttribute()
    {
        $config = Configuraciones::
        where('id', 1)
        ->first();

        $startdate = $this->created_at;
        $expire = strtotime($startdate. ' + '.$config->tiempo_link.' days');
        $today = strtotime("today midnight");

        $datetime1 = new DateTime(date('Y-m-d H:i:s', $today));
        $datetime2 = new DateTime(date('Y-m-d H:i:s', $expire));
        $interval = $datetime1->diff($datetime2);       

        /*+"y": 0
        +"m": 0
        +"d": 12
        +"h": 18
        +"i": 32
        +"s": 44*/
        $hour = 0;
        $addhour = 0;
        if($interval->format('%d') > 0)
            $addhour = $interval->format('%d') * 24;

        $hour = $interval->format('%h') + $addhour;

        if($today >= $expire){
            return "Expirado";
        } else {
            return $hour.":".$interval->format('%i').":".$interval->format('%s');
        }

        return $horasConfig;

        //2020-09-19 | 06:32:44PM

    }
}
