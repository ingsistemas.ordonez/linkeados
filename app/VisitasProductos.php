<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitasProductos extends Model
{
    //
    protected $fillable = [ 
        'productos_id', 'codigo_producto', 'codigo_linkeado', 'visitas'
    ];

}
 