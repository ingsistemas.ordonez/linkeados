<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotosProductos extends Model
{
    //
    //protected $primaryKey = 'idfp';

    protected $fillable = [
        'productos_id', 'url_imagen', 'tipo'
    ];

    public function fotoproductos()
    {
        return $this->belongsTo(Productos::class, 'productos_id');
    }
}
