<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBancarios extends Model {

    protected $table = 'datos_bancarios';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'banco_id', 'nombre', 'cuenta', 'tipo', 'constancia_activa', 'constancia_activa_url', 'correo', 'fecha_pago', 'telefono', 'direccion'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function banco() {
        return $this->belongsTo(Bancos::class);
    }

}
