<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienda extends Model
{
    protected $table = 'tienda';
    //
    protected $fillable = [ 
        'ip_user', 'producto_id', 'sku', 'users_id', 'links_id', 'codigo', 'token' 
    ];
}
