<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'url_perfil', 'ciudad', 'direccion', 'telefono', 'cc_id', 'sitio_web', 'quien_es', 'redes1', 'redes2', 'redes3', 'estado', 'password_reset', 'url_pago'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function categoriacomercio() {
        return $this->belongsTo(CategoriasComercios::class, 'cc_id');
    }

    public function getGetNameAttribute() {
        return strtoupper($this->name);
    }

    /* public function setNameAttribute($value)
      {
      $this->attributes['name'] = strtoupper($value);
      } */

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getFormatFechaAttribute() {
        return $this->created_at->format('d M Y');
    }

    public function getImagenAttribute() {
        return '<img src="' . url('storage/app/public/' . $this->url_perfil) . '" alt="user" class="img-circle" style="width: 150px; height: 131px;display: block;margin-bottom: 30px;">';
    } 

    public function getImagenlistAttribute() {
        if (!empty($this->url_perfil)) {
            return '<img src="' . url('storage/app/public/' . $this->url_perfil) . '" alt="user" class="img-circle float-left m-r-10" style="width: 30px; height: 30px;">';
        } else {
            return '<img src="https://ubisoft-avatars.akamaized.net/a3c1a636-db47-4751-941b-1acc538932de/default_256_256.png" alt="user" class="img-circle float-left m-r-10" style="width: 30px; height: 30px;">';
        }
    }

    public function getEstadosAttribute() {
        return ($this->estado == 'I') ? '<span style="color: #929292">Inactivo</span>' : '<span style="color: #432874">Activo</span>';
    }

    public function productos() {
        return $this->hasMany(Productos::class, 'users_id');
    }

    public function linkeados() {
        return $this->hasMany(Links::class, 'users_id');
    }

}
