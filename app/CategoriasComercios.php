<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CategoriasComercios extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre_cc',
                'onUpdate'=> true 
            ]
        ]; 
    }

    //
    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function getFormatFechaAttribute()
    {
        return $this->created_at->format('d M Y');
    }
}
