<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Productos extends Model
{
    //
    //protected $primaryKey = 'idproductos';

    protected $fillable = [ 
        'nombre_p', 'web', 'url_web', 'descripcion', 'porcentaje_a', 'cp_id', 'codigo', 'users_id', 'valor', 'conversion', 'sku'
    ];

    protected $hidden = [
        'codigo', 'users_id'
    ];

    public function categoriap() 
    {
        return $this->belongsTo(CategoriasProductos::class, 'cp_id');
    }

    public function username() 
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function productofotos()
    {
        return $this->hasMany(FotosProductos::class);
    }

    public function getVisitaspAttribute()
    {   
        $visitas = VisitasProductos::
        where('productos_id', '=', $this->id)
        ->first();

        if($visitas)
            return $visitas->visitas;
        else 
            return 0;
    }

    public function getImagenlistAttribute($value)
    {
        //$fotos = $value->productofotos;
        if(!empty($this->productofotos[0]->url_imagen)) {
            if($this->productofotos[0]->tipo == 'IN')
                $url = url('storage/app/public/'.$this->productofotos[0]->url_imagen);
            else 
                $url = $this->productofotos[0]->url_imagen;

            return '<img src="'.$url.'" alt="user" class="img-circle float-left m-t-5">'; 
        } else {
            return '<img src="https://www.syncros.com/_ui/responsive/common/images/no-product-image-available.png" alt="user" class="img-circle float-left m-t-5">';         
        }
    }

    public function setCodigoAttribute($value) 
    {
        $length = 10;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $this->attributes['codigo'] = $randomString;
    }   

    public function getCalculovalorAttribute()
    {
        $configuracion = Configuraciones::
        where('id', '=', 1)
        ->first();

        return $this->porcentaje_a - (($this->porcentaje_a * $configuracion->comision) / 100);
    }

    public function getYalinkeadoAttribute()
    {
        $links = Links::
        where([
            ['productos_id', '=', $this->id],
            ['users_id', '=', Auth::user()->id],
        ])
        ->first();
        
        if($links && Auth::user()->cc_id == 2){

            $link = $this->url_web.'?codigo='.$this->codigo.'-'.$links->codigo;

            return '<div class="box-link m-t-5 box-linkeado" data-link="'.$link.'">
                        <div style="display:block">
                            <a href="'.$link.'" target="_blank">
                                <i class="fa fa-link float-left"></i> 
                            </a>
                            <div class="float-right">'.$this->codigo.'</div>
                            <div style="clear:both"></div>
                        </div>
                    </div>';
        } else {
            $link = $this->url_web.'?codigo='.$this->codigo;

            return '<a href="'.$link.'" target="_blank">
                        <div class="box-link m-t-5">
                            <div style="display:block">
                                <i class="fa fa-link float-left"></i> 
                                <div class="float-right">'.$this->codigo.'</div>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                    </a>';
        }
    }

}
