<?php //

namespace App\Imports;

use App\Imports\FirstSheetImport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class ProductosImport implements WithMultipleSheets {
    
   
    public function sheets(): array {
        return [
            'Productos' => new FirstSheetImport(),
        ];
    }

}