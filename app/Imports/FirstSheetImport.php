<?php

namespace App\Imports;

use App\Productos;
use App\CategoriasProductos;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\FotosProductos;

class FirstSheetImport implements ToCollection {

    public function collection(Collection $rows) { 
        $importRow = false;
        $errores = '<ul>';
        foreach ($rows as $index => $row) {
            $createProduct = true;
            $item = [
                'users_id' => Auth::user()->id
            ];

            if (strlen($row[0]) > 0 && $importRow) {
                $item['nombre_p'] = $row[0];
                $item['web'] = $row[1];
                $item['url_web'] = $row[2];
                $item['descripcion'] = $row[3];

                if (filter_var($row[4], FILTER_VALIDATE_INT)) {
                    $item['valor'] = $row[4];
                } else {
                    $createProduct = false;
                    $errores .= '<li>El precio debe ser numerico</li>';
                }

                if (filter_var($row[5], FILTER_VALIDATE_FLOAT)) {
                    $item['porcentaje_a'] = $row[5];
                } else {
                    $createProduct = false;
                    $errores .= '<li>La comisión debe ser decimal</li>';
                }

                $categoriaName = $row[6];
                $categoria = CategoriasProductos::where(
                    'nombre_cp','LIKE',$categoriaName.'%'
                )->first();

                $idCategoria = null;
                if ($categoria) {
                    $idCategoria = $categoria->id;
                } else {
                    $createProduct = false;
                    $errores .= '<li>No se encontro la categoria ingresada.</li>';
                }

                $item['cp_id'] = $idCategoria;
                $item['conversion'] = $row[7];
                $item['codigo'] = null;
                $item['sku'] = $row[11];

                if ($createProduct) {
                    $producto = Productos::create($item);
                    $idProducto = $producto->id;
                    if ($producto) {
                        if ($row[8]) {
                            $this->createphoto($idProducto, $row[8]);
                        }
                        if ($row[9]) {
                            $this->createphoto($idProducto, $row[9]);
                        }
                        if ($row[10]) {
                            $this->createphoto($idProducto, $row[10]);
                        }
                    }
                } else {
                    $errores .='</ul>';
                    $index += 1;
                    header("Status: 404 Found");        
                    echo json_encode(
                        array( 
                            'status'=>false, 
                            'message'=>'Error en el archivo, no se logro subir el producto de la fila ('.$index.') por los siguientes errores: <br/>'.$errores,
                        ), JSON_UNESCAPED_UNICODE
                    ); 
                    exit;            
                }
            }
            $importRow = true;
        }
    }

    private function createphoto($idProducto, $url) {
        FotosProductos::create([
            'productos_id' => $idProducto,
            'url_imagen' => $url
        ]);
    }

}
