<?php

namespace App\Http\Controllers;

use App\Links;
use App\Productos;
use App\CategoriasProductos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use App\Http\Requests\LinksRequest; 

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $cc = CategoriasProductos::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();

        /*$links = Links::
        where('users_id', '=', Auth::user()->id)
        ->orderBy('id','DESC')
        ->paginate(10);*/

        $links = Links::query();
        $links->select('links.*');
        $links->leftJoin('productos', 'productos.id', '=', 'links.productos_id');     
        $links->where('links.users_id', '=', Auth::user()->id);  
        
        if (!empty($request->input('nombre')))
            $links->where('productos.nombre_p', 'LIKE', $request->input('nombre').'%'); 

        if (!empty($request->input('cp_id')))
            $links->where('productos.cp_id', '=', $request->input('cp_id')); 
        
        if ($request->input('valor'))
            $links->whereBetween('productos.valor', [$request->input('valor_i'), $request->input('valor_f') ]);

        if (!empty($request->input('orden_p')))
            $links->orderBy('links.id', $request->input('orden_p'));
        else
            $links->orderBy('links.id','DESC');

        $view = $links->paginate(10);

        return view('links.index', ['links' => $view, 'cc' => $cc]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Links  $links
     * @return \Illuminate\Http\Response
     */
    public function show(Links $links)
    {
        //
        dd($links);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Links  $links
     * @return \Illuminate\Http\Response
     */
    public function edit(Links $links)
    {
        //
        dd($links);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Links  $links
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Links $links)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Links  $links
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $query = Links::
        where([
            ['users_id', '=', Auth::user()->id],
            ['id', '=', $request->input('id')]
        ])->delete();

        if($query)
            return back()->with('status', 'Se elimino link con éxito.');
        else    
            return back()->with('warning', 'No se logro eliminar link con éxito.');
    }
}
