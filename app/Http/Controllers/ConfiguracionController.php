<?php

namespace App\Http\Controllers;

use App\Configuraciones;
use Illuminate\Http\Request;

class ConfiguracionController extends Controller
{  
    /**
     * Display the specified resource.
     *
     * @param  \App\Configuraciones  $configuraciones
     * @return \Illuminate\Http\Response
     */
    public function show(Configuraciones $configuraciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Configuraciones  $configuraciones
     * @return \Illuminate\Http\Response
     */
    public function edit(Configuraciones $configuracion)
    {
        //
        return view('configuracion.edit', [ 'configuracion' => $configuracion ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuraciones  $configuraciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Configuraciones $configuracion)
    {
        //
        $configuracion->update($request->all());

        return redirect()->route('configuracion.edit', 1)->with('status', 'Se actualizo con éxito.');  
    }
    
}
