<?php

namespace App\Http\Controllers;

use App\Pagos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->input('estado'));
        //
        $query = Pagos::query();
        
        if(Auth::user()->cc_id == 3) {
            if(!empty($request->input('estado'))) {
                if($request->input('estado') == 'E') {
                    $query->where('estado_afiliado', '=', $request->input('estado'));
                    $query->where('estado_comerciante', '=', $request->input('estado'));
                }
                if($request->input('estado') == 'EA') 
                    $query->where('estado_afiliado', '=', 'EA');
                if($request->input('estado') == 'EAC') 
                    $query->where('estado_comerciante', '=', 'EA');
                if($request->input('estado') == 'C') {
                    $query->where('estado_afiliado', '=', $request->input('estado'));
                    $query->where('estado_comerciante', '=', $request->input('estado'));
                }
            }
        } elseif(Auth::user()->cc_id == 2) {// si es un afiliado
            $query->where('users_id', '=', Auth::user()->id);
            
            if(!empty($request->input('estado')))
                $query->where('estado_afiliado', '=', $request->input('estado'));
           /* $pagos = Pagos::
            where('users_id', '=', Auth::user()->id)
            ->orderBy('id','DESC')
            ->get();*/
        } elseif(Auth::user()->cc_id == 1) {    
            $query->select('pagos.*');
            $query->leftJoin('productos', 'productos.id', '=', 'pagos.productos_id');

            if(!empty($request->input('estado')))
                $query->where('estado_comerciante', '=', $request->input('estado'));

            $query->where([
                ['productos.users_id', '=', Auth::user()->id],
                //['pagos.estado_afiliado', '=', 'EA']
            ]);

            /*$pagos = Pagos::
            select('pagos.*')
            ->leftJoin('productos', 'productos.id', '=', 'pagos.productos_id')
            ->where([
                ['productos.users_id', '=', Auth::user()->id],
                //['pagos.estado_afiliado', '=', 'EA']
            ])
            ->orderBy('pagos.id','DESC')
            ->get();*/
        }        

        $query->orderBy('pagos.id','DESC');
        $pagos = $query->get();

        return view('pagos.index', compact('pagos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function show(Pagos $pagos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function edit(Pagos $pagos)
    {
        //
        //dd($pagos);
        return view('pagos.edit', [ 'pago' => $pagos ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pagos $pagos)
    {
        //
        //dd($pagos);
        $pagos->update($request->all());

        return back()->with('status', 'Actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pagos  $pagos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pagos $pagos)
    {
        //
    }

    //Soliticar pago de afiliado
    public function spa(Request $request)
    {
        foreach($request->input('estados') as $item) {
            Pagos::where('id', $item)
            ->update(
                ['estado_afiliado' => 'EA']
            );
        }

        return redirect()->route('pagos.index')->with('status', 'Los pagos fueron cambiados con éxito.'); 
    }

    //Soliticar liquidar pagos de comerciante
    public function spl(Request $request)
    {   
        foreach($request->input('estados') as $item) {
            Pagos::where([
                ['id', '=', $item]
            ]) 
            ->update(
                ['estado_comerciante' => 'EA']
            );
        }

        return redirect()->route('pagos.index')->with('status', 'Los pagos fueron cambiados con éxito.'); 
    }
}
