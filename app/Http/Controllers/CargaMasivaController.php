<?php

namespace App\Http\Controllers;

use App\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CargaMasivaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('cargamasiva.index');
    }
    
    public function indexusers() {
        return view('cargamasiva.indexusers');
    }

    public function process(Request $request) {
        try {
            foreach ($request->all() as $key => $value) {
                if ($request->hasFile($key)) {
                    $attach = $request->file($key);

                    if ($attach->extension() === "xlsx") {
                        $res = Excel::import(new \App\Imports\ProductosImport, $attach);
                        return response()->json(['message' => 'El archivo se subio correctamente', 'status' => $res], 200);
                    }
                }
            }

            return response()->json(['message' => 'No se cargo ningun archivo.', 'status' => false], 404);
        } catch (Exception $ex) {
            return response()->json(['message' => 'Se presento un error,' . $ex->getMessage() . '. Intente más tarde.', 'status' => false],404);
        }
    }
    
    public function processusers(Request $request) {
        try {
            foreach ($request->all() as $key => $value) {
                if ($request->hasFile($key)) {
                    $attach = $request->file($key);

                    if ($attach->extension() === "xlsx") {
                        Excel::import(new \App\Imports\UsersImport, $attach);
                        return response()->json(['message' => '', 'status' => true]);
                    }
                }
            }

            return response()->json(['message' => 'No se cargo ningun archivo.', 'status' => false]);
        } catch (Exception $ex) {
            return response()->json(['message' => 'Se presento un error,' . $ex->getMessage() . '. Intente más tarde.', 'status' => false]);
        }
    }

    public function downloadtemplate() {
        $file = public_path() . '/download/template.xlsx';
        return response()->download($file);
    }
    
    public function downloadtemplateusers() {
        $file = public_path() . '/download/templateusers.xlsx';
        return response()->download($file);
    }

}
