<?php

namespace App\Http\Controllers;

use App\User;
use App\CategoriasComercios;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function _construct()
    {
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::withCount('productos')
        //->where('id', '>=', '20')
        ->orderBy('users.id','DESC')
        //->take()
        ->get();

        //https://laravel.com/docs/7.x/eloquent-collections
        //->all()
        //->find(4)
        //->load('categoriacomercio')
        //->toArray()
        //->toJson()
        //->with('categoriacomercio')
        
        return view('users.index', compact('users'));

        /*return view('users.index', [
            'users' => $users
        ]);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cc = CategoriasComercios::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();

        return view('users.create', [ 'cc' => $cc ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //
        //dd($request->all());
        /*
        $request->validate([
            'name'      => 'required',
            'email'     => ['required','email','unique:users'],
            'password'  => ['required','min:8']
        ]);*/
        
        /*$user = User::create([
            //'user_id' => auth()->user()->id //para registrar el id del usuario login
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);*/

        $user = User::create($request->all());

        //imagen
        if($request->file('url_perfil')) {
            $user->url_perfil = $request->file('url_perfil')->store('users', 'public');
            $user->save();
        }

        return back()->with('status', 'Creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        if(Auth::user()->cc_id == 1) {// si es un comerciante
            if($user->id == Auth::user()->id) 
                return view('users.view', ['user' => $user]);
            else
                return view('users.aviso', ['user' => $user]);
        } else if(Auth::user()->cc_id == 2) { //es un afiliado
            if($user->id == Auth::user()->id) 
                return view('users.view', ['user' => $user]);
            else
                return view('users.aviso', ['user' => $user]);
        } else {
            return view('users.view', ['user' => $user]);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //  
        //dd($user);
        if(Auth::user()->cc_id == 1 || Auth::user()->cc_id == 2) {
            if($user->id != Auth::user()->id)
                return view('users.aviso');
        }

        $cc = CategoriasComercios::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();
        
        return view('users.edit', ['user' => $user, 'cc' => $cc ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        //dd($request->cc_id);
        //
        /*if(!empty($request->password)) {
            if($request->password != $request->password_confirmation){
                return back()->errors()->add('event', 'La contraseña no son iguales.');
            }
        }*/

        $user->update($request->all());

        //imagen
        if($request->file('url_perfil_new')) {
            //eliminar imagen
            Storage::disk('public')->delete($user->url_perfil);
            $user->url_perfil = $request->file('url_perfil_new')->store('users', 'public');
            $user->save();
        }

        return redirect()->route('users.edit', $user)->with('status', 'Actualizado con éxito.');
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        Storage::disk('public')->delete($user->url_perfil);
        $user->delete();

        return back()->with('status', 'Se elimino cliente con éxito.');  
    }

    public function updateurl(Request $request)
    {        
        $user = User::find(Auth::user()->id);
        // Make sure you've got the Page model
        if($user) {
            $user->url_pago = $request->input('url_pago');
            $user->save();
        }

        return redirect()->route('perfil.edit')->with('status', 'Se actualizo el link de pago correctamente.');  
    }
}
