<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\VisitasProductos;
use App\Productos;
use App\Pagos;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $query = Productos::query();
        $query->select('visitas_productos.visitas');
        $query->leftJoin('visitas_productos', 'productos.id', '=', 'visitas_productos.productos_id');
        
        if(Auth::user()->cc_id == 1) {
            $query->where('productos.users_id', '=', Auth::user()->id);
        }
        
        if(Auth::user()->cc_id == 2) {
            $query->leftJoin('links', 'productos.id', '=', 'links.productos_id');
            $query->where('links.users_id', '=', Auth::user()->id);
        }

        $visitas = $query->sum('visitas_productos.visitas');

        /*---------Pagos ----------------------*/
        $query = Pagos::query();
        $query->leftJoin('productos', 'productos.id', '=', 'pagos.productos_id');
        //$query->where('pagos.estado_afiliado', '=', 'C');
        //$query->where('pagos.estado_comerciante', '=', 'C');

        if(Auth::user()->cc_id == 1) {    
            $query->where('productos.users_id', '=', Auth::user()->id);
            $ventas = $query->sum('productos.valor'); 
        }
        if(Auth::user()->cc_id == 2) {
            $query->where('pagos.users_id', '=', Auth::user()->id);
            $query->where('pagos.estado_afiliado', '!=', 'R');
            $ventas = $query->sum('pagos.comision_afiliado');
        }
        if(Auth::user()->cc_id == 3) {
            $query->where('pagos.estado_afiliado', '!=', 'R');
            $query->where('pagos.estado_comerciante', '!=', 'R');
            $ventas = $query->sum('pagos.comision_admin');
        } 
        
        /*----------------Comision-------------------------*/
        $query = Pagos::query();
        $query->leftJoin('productos', 'productos.id', '=', 'pagos.productos_id');

        if(Auth::user()->cc_id == 1) {    
            $query->where('productos.users_id', '=', Auth::user()->id);
            $query->whereIn('pagos.estado_comerciante', ['E','EA']);
            $comisionA = $query->sum('pagos.comision_admin');
            $comisionAF = $query->sum('pagos.comision_afiliado');

            $comision = $comisionA + $comisionAF;
        }
        if(Auth::user()->cc_id == 2) {
            $query->where('pagos.users_id', '=', Auth::user()->id);
            $query->whereIn('pagos.estado_afiliado', ['E','EA']);
            $comision = $query->sum('pagos.comision_afiliado');
        }
        if(Auth::user()->cc_id == 3) {
            $query->whereIn('pagos.estado_comerciante', ['E','EA']);
            $comision = $query->sum('pagos.comision_afiliado');
        }

        /*------------Ventas por mes --------------------*/
        /*$query = Pagos::query();
        $query->selectRaw("MONTH(created_at) AS MONTH, COUNT(*) AS TOTAL");
        $query->whereBetweenRaw("MONTH(created_at)", ["MONTH(CURDATE() - INTERVAL 6 MONTH)", "MONTH(CURDATE())"]);
        $query->groupBy("MONTH");
        $ventasmes = $query->get();*/

        if(Auth::user()->cc_id == 3) {  
            $ventasmes = DB::select("SELECT COUNT(*) AS TOTAL_ACTIONS, DATE_FORMAT(created_at,'%b') as MES
                                    FROM pagos
                                    WHERE MONTH(created_at) BETWEEN MONTH(CURDATE() - INTERVAL 6 MONTH) AND MONTH(CURDATE())
                                    GROUP BY MES");
            $topproductos = DB::select("SELECT count(1) as numberSold, productos.nombre_p 
                                    FROM pagos 
                                    LEFT JOIN productos ON productos.id = pagos.productos_id 
                                    group by nombre_p 
                                    order by count(1) desc
                                    LIMIT 5");

            $topcategorias = DB::select("SELECT count(1) as numberSold, cp.nombre_cp 
                                        FROM pagos 
                                        LEFT JOIN productos ON productos.id = pagos.productos_id 
                                        LEFT JOIN categorias_productos AS cp ON cp.id = productos.cp_id 
                                        group by cp.nombre_cp 
                                        order by count(1) desc");
        }

        if(Auth::user()->cc_id == 2) {  
            $ventasmes = DB::select("SELECT COUNT(*) AS TOTAL_ACTIONS, DATE_FORMAT(created_at,'%b') as MES
                        FROM pagos
                        WHERE MONTH(created_at) BETWEEN MONTH(CURDATE() - INTERVAL 6 MONTH) AND MONTH(CURDATE())
                        AND pagos.users_id = ?
                        GROUP BY MES", [Auth::user()->id]);
            $topproductos = DB::select("SELECT count(1) as numberSold, productos.nombre_p 
                                        FROM pagos 
                                        LEFT JOIN productos ON productos.id = pagos.productos_id 
                                        WHERE pagos.users_id = ?
                                        group by nombre_p 
                                        order by count(1) desc
                                        LIMIT 5", [Auth::user()->id]);
            $topcategorias = DB::select("SELECT count(1) as numberSold, cp.nombre_cp 
                                        FROM pagos 
                                        LEFT JOIN productos ON productos.id = pagos.productos_id 
                                        LEFT JOIN categorias_productos AS cp ON cp.id = productos.cp_id 
                                        WHERE pagos.users_id = ?
                                        group by cp.nombre_cp 
                                        order by count(1) desc", [Auth::user()->id]);
        }        

        if(Auth::user()->cc_id == 1) {  
            $ventasmes = DB::select("SELECT COUNT(*) AS TOTAL_ACTIONS, DATE_FORMAT(pagos.created_at,'%b') as MES
                                    FROM pagos
                                    LEFT JOIN productos ON productos.id = pagos.productos_id
                                    WHERE MONTH(pagos.created_at) BETWEEN MONTH(CURDATE() - INTERVAL 6 MONTH) AND MONTH(CURDATE())
                                    AND productos.users_id = ?
                                    GROUP BY MES", [Auth::user()->id]);
            $topproductos = DB::select("SELECT count(1) as numberSold, productos.nombre_p 
                                    FROM pagos 
                                    LEFT JOIN productos ON productos.id = pagos.productos_id 
                                    WHERE productos.users_id = ?
                                    group by nombre_p 
                                    order by count(1) desc
                                    LIMIT 5", [Auth::user()->id]);

            $topcategorias = DB::select("SELECT count(1) as numberSold, cp.nombre_cp 
                                        FROM pagos 
                                        LEFT JOIN productos ON productos.id = pagos.productos_id 
                                        LEFT JOIN categorias_productos AS cp ON cp.id = productos.cp_id
                                        WHERE productos.users_id = ? 
                                        group by cp.nombre_cp 
                                        order by count(1) desc", [Auth::user()->id]);
        }

        //dd($topproductos);

        return view('welcome', ['visitas'=>$visitas, 'ventas'=>round($ventas,2), 'comision'=>round($comision,2),'ventasmes'=>$ventasmes, 'topproductos' => $topproductos, 'topcategorias' => $topcategorias]);
        //return 'hola mundo';
    }

    
}
