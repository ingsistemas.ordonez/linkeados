<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Shopify;

class PerfilController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function edit() {
        $user = User::find(Auth::user()->id);
        $shopify = Shopify::where('users_id', '=', Auth::user()->id)->first();

        return view('perfil.edit', ['user' => $user, 'shopify' => $shopify]);
    }

    public function editprocess(Request $request) {

        try {
            $user = User::find(Auth::user()->id);
            $user->name = $request->get('name');
            $user->ciudad = $request->get('ciudad');
            $user->direccion = $request->get('direccion');
            $user->telefono = $request->get('telefono');
            $user->email = $request->get('email');
            $user->sitio_web = $request->get('sitio_web');
            $user->quien_es = $request->get('quien_es');
            $user->redes1 = $request->get('redes1');
            $user->redes2 = $request->get('redes2');
            $user->redes3 = $request->get('redes3');

            if ($request->file('img_perfil')) {
                $user->url_perfil = $request->file('img_perfil')->store('users', 'public');
            }

            $user->save();

            return response()->json(['message' => 'Datos actualizados.', 'status' => true]);
        } catch (Exception $exc) {
            return response()->json(['message' => 'Se presento un error mientras se intentaba actualizar los datos del usuario.', 'status' => false]);
        }
    }

    public function setshopify(Request $request)
    {   
        $res = Shopify::where('llave', '=', $request->llave)->first();
        if($res) {
            if($res->users_id != Auth::user()->id)
                return redirect()->route('perfil.edit')->with('warning', 'El token ingresado ya esta registrado con otro perfil.');    
            //return back()->with('error', 'El token ingresado ya esta registrado con otro perfil.'); 
        }

        $res = Shopify::where('users_id', '=', Auth::user()->id)->first();

        $calculated_hmac = base64_encode(hash_hmac('sha256', 0, '772822004e0110768bb2fc382a501b485253444320844919424f190df052e6c9', true));

        $urlP = explode('products',$request->url_tienda);
        $url = $urlP[0].'products/';

        if(!$res)
            Shopify::create([
                'users_id' => Auth::user()->id,
                'nombre' => $request->nombre,
                'llave' => $request->llave,
                'url_tienda' => $request->url_tienda,
                'verificado' => 'NO'
            ]); 
        else {
            $res->nombre = $request->nombre;
            //$res->nombre = $calculated_hmac;
            $res->llave = $request->llave;
            $res->url_tienda = $url;
            $res->verificado = 'NO';

            $res->save();
        }
        
        return back()->with('status', 'Ingresado con éxito.'); 
    }

}
