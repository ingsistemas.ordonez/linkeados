<?php

namespace App\Http\Controllers;

use App\Productos;
use App\CategoriasProductos;
use App\FotosProductos;
use App\Links;
use App\User;
//use Illuminate\Http\Request;
use App\Http\Requests\ProductosRequest;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        //$name = $request->input('search');
        //dd($name);

        $cc = CategoriasProductos::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();

        if(Auth::user()->cc_id == 1) {// si es un comerciante
            
            $productos = Productos::query();
            $productos->where('users_id', '=', Auth::user()->id);
            
            if (!empty($request->input('search'))) {

                $comercio = User::
                where([
                    ['name', 'LIKE', $request->input('search').'%'], 
                    ['cc_id', '=', 1] 
                ])
                ->first();

                if($comercio){
                    $productos->where('users_id', '=', $comercio->id);  
                } else {                            
                    if(is_numeric($request->input('search')))
                        $productos->where('id', '=', $request->input('search'));  
                    else
                        $productos->where('nombre_p', 'LIKE', $request->input('search').'%');  
                }
            }                
                
            if (!empty($request->input('cp_id')))
                $productos->where('cp_id', '=', $request->input('cp_id'));  

            if (!empty($request->input('porcentaje_a')))
                $productos->where('porcentaje_a', '=', $request->input('porcentaje_a'));
                
            if ($request->input('valor'))
                $productos->whereBetween('productos.valor', [ $request->input('valor_i'), $request->input('valor_f') ]);
    
            if (!empty($request->input('orden_p')))
                $productos->orderBy('id', $request->input('orden_p'));
            else
                $productos->orderBy('id','DESC');

            $view = $productos->paginate(10);

            return view('productos.comerciante.index', ['productos' => $view, 'cc' => $cc ]);   

        } else if(Auth::user()->cc_id == 2) { //afiliado  

            $productos = Productos::query();
            //$productos->where('users_id', '=', Auth::user()->id);
            
            if (!empty($request->input('search'))) {

                $comercio = User::
                where([
                    ['name', 'LIKE', $request->input('search').'%'], 
                    ['cc_id', '=', 1] 
                ])
                ->first();

                if($comercio){
                    $productos->where('users_id', '=', $comercio->id);  
                } else {                            
                    if(is_numeric($request->input('search')))
                        $productos->where('id', '=', $request->input('search'));  
                    else
                        $productos->where('nombre_p', 'LIKE', $request->input('search').'%');  
                }
            }                  
                
            if (!empty($request->input('cp_id')))
                $productos->where('cp_id', '=', $request->input('cp_id'));  

            if (!empty($request->input('porcentaje_a')))
                $productos->where('porcentaje_a', '=', $request->input('porcentaje_a'));
                
            if ($request->input('valor'))
                $productos->whereBetween('valor', [$request->input('valor_i'), $request->input('valor_f') ]);
    
            if (!empty($request->input('orden_p')))
                $productos->orderBy('id', $request->input('orden_p'));
            else
                $productos->orderBy('id','DESC');

            $view = $productos->paginate(10);

            return view('productos.afiliado.index', ['productos' => $view, 'cc' => $cc ]);   

        } else {    

            /*if(!empty($request->input('search')))
                $productos = Productos::
                where('nombre_p','LIKE', $request->input('search').'%')
                ->orderBy('id','DESC')
                ->get();
            else     
                $productos = Productos::
                orderBy('id','DESC')
                ->get();  */

            $productos = Productos::query();
            //$productos->where('users_id', '=', Auth::user()->id);                        
            
            if (!empty($request->input('search'))) {

                $comercio = User::
                where([
                    ['name', 'LIKE', $request->input('search').'%'], 
                    ['cc_id', '=', 1] 
                ])
                ->first();

                if($comercio){
                    $productos->where('users_id', '=', $comercio->id);  
                } else {                            
                    if(is_numeric($request->input('search')))
                        $productos->where('id', '=', $request->input('search'));  
                    else
                        $productos->where('nombre_p', 'LIKE', $request->input('search').'%');  
                }
            }        

            $view = $productos->get();

            return view('productos.index', ['productos' => $view, 'cc' => $cc ]);
        }            
        //dd(Auth::user()->cc_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cc = CategoriasProductos::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();

        if(Auth::user()->cc_id == 3) //si es un admin
            return view('productos.create', [ 'cc' => $cc ]);
        else
            return view('productos.comerciante.create', [ 'cc' => $cc ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductosRequest $request)
    {       
        $producto = Productos::create($request->all() + ['codigo' => 1, 'tipo' => 'ADMIN', 'users_id' => $request->user()->id ]);        
        
        foreach($request->file('url_imagen') as $img) {
            FotosProductos::create([
                'productos_id' => $producto->id,
                'url_imagen' => $img->store('productos', 'public')
            ]);
        }

        return back()->with('status', 'Creado con éxito.');
    }

   
    /**
     * Display the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $producto)
    {
        //dd($producto);

        return view('productos.afiliado.view', ['producto' => $producto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $producto)
    {
        //
        $cc = CategoriasProductos::
        //->where('id', '>=', '20')
        orderBy('id','DESC')
        //->take()
        ->get();

        if(Auth::user()->cc_id == 3) //si es un admin
            return view('productos.edit', [ 'producto' => $producto, 'cc' => $cc ]);
        else
            return view('productos.comerciante.edit', [ 'producto' => $producto, 'cc' => $cc ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(ProductosRequest $request, Productos $producto)
    {
        //
        //dd($request);

        $producto->update($request->all());

        /*foreach($producto->productofotos as $item) {
            Storage::disk('public')->delete($item->url_imagen);
        }*/      

        FotosProductos::where('productos_id', $producto->id)->delete();

        if(is_array($request->url_imagen_old)) {
            foreach($request->url_imagen_old as $img) {
                FotosProductos::create([
                    'productos_id' => $producto->id,
                    'url_imagen' => $img
                ]);
            }
        }

        if($request->file('url_imagen')) {
            foreach($request->file('url_imagen') as $img) {
                FotosProductos::create([
                    'productos_id' => $producto->id,
                    'url_imagen' => $img->store('productos', 'public')
                ]);
            }
        }

        return back()->with('status', 'Actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $producto)
    {
        //
        if($producto->productofotos) {
            foreach($producto->productofotos as $item) {
                Storage::disk('public')->delete($item->url_imagen);
            }       

            FotosProductos::where('productos_id', $producto->id)->delete();
        }

        $producto->delete();

        return redirect()->route('productos.index')->with('status', 'Se elimino producto con éxito.');  
    }

    public function duplicate(Productos $producto)
    {
        //dd($producto->productofotos);   
        
        $duplicado = Productos::create([
            'users_id' => Auth::user()->id,
            'codigo' => 1,
            'nombre_p' => $producto['nombre_p'],
            'web' => $producto['web'],
            'url_web' => $producto['url_web'],
            'descripcion' => $producto['descripcion'],
            'valor' => $producto['valor'],
            'porcentaje_a' => $producto['porcentaje_a'],
            'cp_id' => $producto['cp_id'],
            'tipo' => $producto['tipo'],
            'conversion' => $producto['conversion']
        ]);   

        foreach($producto->productofotos as $item) {  

            $ext = explode(".", $item['url_imagen']);
            $file = 'public/'.$item['url_imagen'];      

            $imagenew = 'productos/duplicado_'.uniqid().'.'.$ext[1];

            Storage::copy($file, 'public/'.$imagenew);

            FotosProductos::create([
                'productos_id' => $duplicado->id,
                'url_imagen' => $imagenew
            ]);
        }

        return redirect()->route('productos.index')->with('status', 'El producto se duplico con éxito.'); 
    }

    public function linkear(Productos $producto)
    {                
        //dd($producto->id);

        $links = Links::
        where([
            ['productos_id', '=', $producto->id],
            ['users_id', '=', Auth::user()->id],
        ])
        ->get();
        
        if(count($links) > 0){
            return redirect()->route('productos.show', $producto)->with('warning', 'Este producto ya fue agregado a tu lista.');  
        } else {
            Links::create([
                'codigo' => 1,
                'productos_id' => $producto->id,
                'users_id' => Auth::user()->id
            ]);

            return redirect()->route('links.index')->with('status', 'Se agrego el producto a lista con éxito.');  
        }    
    }
}
