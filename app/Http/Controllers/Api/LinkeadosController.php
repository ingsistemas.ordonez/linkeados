<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Productos;
use App\Links;
use App\VisitasProductos;
use App\Pagos;
use App\Configuraciones;
use App\Logs;
use App\CategoriasProductos;
use App\FotosProductos;
use App\Shopify;
use App\Tienda;

use Illuminate\Http\Request;

class LinkeadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        //
        $productos = Productos::
        //->where('id', '>=', '20')
        orderBy('id','ASC')
        ->get();

        return response()->json($productos);
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function visto(Request $request)
    {
        //
        /*$headers = getallheaders();
        Logs::create([
            'tipo' => 'VISTO',
            'log' => serialize($headers)
        ]); */

        $codigos = explode('-', $request->input('codigo'));
        $bkcodigo = $request->input('codigo');

        $links = Links::
        where('codigo', '=', $codigos[1])
        ->first();

        if($links) {

            $producto = Productos::
            where('id', '=', $links->productos_id)
            ->first();

            Tienda::where('codigo', '=', $bkcodigo)->delete();

            Tienda::create([
                'ip_user' => $request->input('ipuser'),
                'users_id' => $links->users_id,
                'links_id' => $links->id,
                'producto_id' => $links->productos_id,
                'sku' => $producto->sku,
                'codigo' => $bkcodigo
            ]);

            $visitas = VisitasProductos::
            where('productos_id', '=', $links['productos_id'])
            ->first();

            if(!$visitas) {
                $cont = 1;    
                VisitasProductos::create([
                    'productos_id' => $links['productos_id'],
                    'codigo_producto' => $codigos[0],
                    'codigo_linkeado' => $codigos[1],
                    'visitas' => $cont
                ]);   
            } else {
                $cont = $visitas['visitas'] + 1; 
                $visitas->update(['visitas' => $cont]);                
            }       

            return response()->json(true, 200);
        } else {
            return response()->json(null, 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productos $productos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos)
    {
        //
    }

    public function tiendalinkeado(Request $request)
    {   
        $headers = getallheaders();
        Logs::create([
            'tipo' => 'TIENDALINKEADO',
            'log' => serialize($headers)
        ]);

        $codigos = $request->input('codigos');   

        if(empty($codigos) || $codigos == 'null')
            return response()->json('Error: se requiere codigo de productos', 400); 

        $healthy = array("https://", "/");
        $yummy   = array("");

        $string = str_replace($healthy, $yummy, $request->input('url_current'));
        $find = str_replace($healthy, $yummy,$request->input('urlpago'));
        $pos = strpos($string, $find);
        
        if($pos !== false)
            $var = 'OK';         
        else
            return response()->json('Error: no coincide la url de pago:'.$string.'->'.$find, 400);             
        
        //Elimina toda la tienda con el ip usuario
        Tienda::where('ip_user', '=', $request->input('ipuser'))->delete();

        foreach( $codigos as $codigo ) {

            $bkcodigo = $codigo;
            $codigo = explode('-', $codigo);

            $links = Links::
            where('codigo', '=', $codigo[1])
            ->first();

            if($links) {
                
                $producto = Productos::
                where('id', '=', $links->productos_id)
                ->first();

                Tienda::create([
                    'ip_user' => $request->input('ipuser'),
                    'users_id' => $links->users_id,
                    'links_id' => $links->id,
                    'producto_id' => $links->productos_id,
                    'sku' => $producto->sku,
                    'codigo' => $bkcodigo
                ]);               
            } 

            return response()->json('Se ingreso el pago', 200);   
        } 
    }

    public function newproducto() 
    {
        $headers = getallheaders();

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');          
        $datos = json_decode($data);     
        
        $shopify = Shopify::where('nombre', '=', $datos->vendor)->first();    

        if($shopify) {
        
            $verified = $this->verify_webhook($data, $hmac_header, $shopify->llave);  

            if(!$verified) {
                Logs::create([
                    'log' => 'Llave incorrecta: '.$shopify->llave
                ]); 
                return response()->json('Llave Incorrecta', 400);   
            }

            $producto = Productos::create([
                'users_id' => $shopify->users_id,
                'codigo' => 1, 
                'nombre_p' => $datos->title,
                'url_web' => $shopify->url_tienda.$datos->handle,
                'descripcion' => strip_tags($datos->body_html),
                'valor' => $datos->variants[0]->price,
                'porcentaje_a' => 0,
                'cp_id' => $this->searchCategoria($datos->product_type),
                'tipo' => 'ADMIN', 
                'conversion' => 'venta',
                'sku' => $datos->variants[0]->sku
            ]);    
            
            if(!$producto)
                return response()->json('Producto no creado', 400);   
                
            foreach($datos->images as $img) {
                FotosProductos::create([
                    'productos_id' => $producto->id,
                    'tipo' => 'OUT',
                    'url_imagen' => $img->src
                ]);
            }

            return response()->json('Producto creado', 200);  

        } else {
            Logs::create([
                'log' => 'No se encontro el vendor: '.$data->vendor,
                'tipo' => 'NUEVOPRODUCTO'
            ]); 
            return response()->json('No se encontro el vendor', 400); 
        }
    }

    public function index(Request $request)
    {          
        $headers = getallheaders();

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');        
       
        //$verified = $this->verify_webhook($data, $hmac_header);

        Logs::create([
            'log' => $data,
            'tipo' => 'CARRITO'
        ]);

        //error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result

        return response()->json('Se ingreso el pago', 200);   
    }   

    public function tiendashopify(Request $request)
    {          
        $headers = getallheaders();
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');  
 
        /*Logs::create([
            'tipo' => 'TIENDASHOPIFY',
            'log' => serialize($headers)
        ]);*/
        
        //$data = '{"id":"ccde1aef7aa587fd40a4ec5d897fe42f","token":"ccde1aef7aa587fd40a4ec5d897fe42f","line_items":[{"id":37096987230384,"properties":null,"quantity":1,"variant_id":37096987230384,"key":"37096987230384:2ce978cf9c281eb21b7d62ff88313b83","discounted_price":"1.00","discounts":[],"gift_card":false,"grams":0,"line_price":"1.00","original_line_price":"1.00","original_price":"1.00","price":"1.00","product_id":5958643318960,"sku":"SKU123465","taxable":true,"title":"Compra de un perro","total_discount":"0.00","vendor":"linkeadotienda","discounted_price_set":{"shop_money":{"amount":"1.0","currency_code":"COP"},"presentment_money":{"amount":"1.0","currency_code":"COP"}},"line_price_set":{"shop_money":{"amount":"1.0","currency_code":"COP"},"presentment_money":{"amount":"1.0","currency_code":"COP"}},"original_line_price_set":{"shop_money":{"amount":"1.0","currency_code":"COP"},"presentment_money":{"amount":"1.0","currency_code":"COP"}},"price_set":{"shop_money":{"amount":"1.0","currency_code":"COP"},"presentment_money":{"amount":"1.0","currency_code":"COP"}},"total_discount_set":{"shop_money":{"amount":"0.0","currency_code":"COP"},"presentment_money":{"amount":"0.0","currency_code":"COP"}}}],"note":null,"updated_at":"2020-10-29T22:09:23.165Z","created_at":"2020-10-29T21:42:43.444Z"}';

        $datos = json_decode($data);   
        
        if(count($datos->line_items) == 0)
            return response()->json('No tiene productos agregados', 400); 

        Logs::create([
            'log' => $data,
            'tipo' => 'CARRITO5'
        ]); 
        
        $shopify = Shopify::where('nombre', '=', $datos->line_items[0]->vendor)->first();  

        if($shopify) {
        
            $verified = $this->verify_webhook($data, $hmac_header, $shopify->llave);  

            if(!$verified) { 
                Logs::create([
                    'log' => 'Llave incorrecta: '.$shopify->llave
                ]); 
                return response()->json('Llave Incorrecta', 400);   
            }

            $tienda = Tienda::query();

            foreach($datos->line_items as $compra) {
                $tienda->where('sku', '=', $compra->sku);
                $res = $tienda->first();
                if($res) {
                    $res->token = $datos->token;
                    $res->update();
                }
            }
        }
    }

   
    public function pagoshopify(Request $request)
    {          
        $headers = getallheaders();

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');    

        $datos = json_decode($data); 

        Logs::create([
            'log' => $data,
            'tipo' => 'PAGO3'
        ]); 
        
        $shopify = Shopify::where('nombre', '=', $datos->line_items[0]->vendor)->first();     

        if($shopify) {
        
            $verified = $this->verify_webhook($data, $hmac_header, $shopify->llave);  

            if(!$verified) {
                Logs::create([
                    'log' => 'Llave incorrecta: '.$shopify->llave
                ]); 
                return response()->json('Llave Incorrecta', 400);   
            }

            $tienda = Tienda::query();
            $tienda->where('token', '=', $datos->cart_token);
            
            $skus = [];
            foreach($datos->line_items as $compra) {
                if(!empty($compra->sku))
                    array_push($skus, array('sku', '=', $compra->sku));
            }
            $tienda->where($skus);
            
            $tiendas = $tienda->get();

            $config = Configuraciones::
            where('id', '=', 1)
            ->first();

            foreach($tiendas as $item) {         
                
                $producto = Productos::
                where('id', '=', $item->producto_id)
                ->first();

                $comisionAdmin = ($producto->porcentaje_a * $config->comision) / 100;
                $comisionAfiliado = $producto->porcentaje_a - $comisionAdmin;

                $totalA = ($producto->valor * $comisionAfiliado) / 100;
                $totalAdmin = ($producto->valor * $comisionAdmin) / 100;

                Pagos::create([
                    'users_id' => $item->users_id,
                    'links_id' => $item->links_id,
                    'productos_id' => $item->producto_id,
                    'comision_afiliado' => $totalA,
                    'comision_admin' => $totalAdmin,
                    'codigo_pago' => $item->codigo,
                    'trama' => $data,
                    'estado_afiliado' => 'E',
                    'estado_comerciante' => 'E'
                ]); 
            }

            //vaciar la tienda
            Tienda::where('ip_user', $datos->browser_ip)->delete();

            return response()->json('Se ingreso el pago', 200); 

        } else {

            $shopify = Shopify::where('url_tienda', 'LIKE', '%'.$headers['X-Shopify-Shop-Domain'].'%')->first();     

            $verified = $this->verify_webhook($data, $hmac_header, $shopify->llave);  

            if(!$verified) {
                Logs::create([
                    'log' => serialize($headers),
                    'tipo' => 'NOVENDEDOR'
                ]);  
            } else {
                Logs::create([
                    'log' => serialize($headers),
                    'tipo' => 'VERIFICADOK'
                ]); 

                $shopify->verificado = 'SI';
                $shopify->save();
            }
           
            return response()->json('No se encontro el vendor', 400); 
        }
    }   

    private function verify_webhook($data, $hmac_header, $llave)
    {
        //$SHOPIFY_APP_SECRET = Shopify::where('nombre', '=', 'nombre')->first();
        //$calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_APP_SECRET, true));
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $llave, true));        
        return hash_equals($hmac_header, $calculated_hmac);
    }

    private function searchCategoria($value)
    {
        $res = CategoriasProductos::
        where([
            ['nombre_cp', 'LIKE', $value.'%']
        ])
        ->first();

        if($res)
            return $res->id;
        else
            return 1;
    }
}
