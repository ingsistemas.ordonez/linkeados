<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tipoerror;
use App\Soporte;
use App\SoporteAdjuntos;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\EmailLinkiados;

class SoporteController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        $tipoError = Tipoerror::orderBy('id')->get();
        return view('soporte.create', ['tipoError' => $tipoError]);
    }

    public function createprocess(Request $request) {

        try {
            return DB::transaction(function() use($request) {
                        $infoUploads = $this->uploadFiles($request);
                        $dataSoporte = [
                            'user_id' => Auth::user()->id,
                            'tipoerror_id' => $request['tipo_error'],
                            'nombre' => $request['nombre'],
                            'comentario' => $request['comentario'],
                            'correo' => $request['correo'],
                            'telefono' => $request['telefono'],
                            'sitio_web' => $request['sitio_web']
                        ];
                        $soporte = Soporte::create($dataSoporte);

                        foreach ($infoUploads as $key => $value) {
                            SoporteAdjuntos::create([
                                'nombre' => $key,
                                'url' => $value,
                                'soporte_id' => $soporte->id
                            ]);
                        }

                        $case = str_pad($soporte->id, 6, "0", STR_PAD_LEFT);

                        $emailLinkiados = new EmailLinkiados([Auth::user()->email], 'Soporte linkiados', 'emails.soporte', [
                            'nombre' => Auth::user()->name,
                            'caso' => $case
                        ]);
                        $this->addImgAttachEmail($infoUploads, $emailLinkiados);
                        $emailLinkiados->send();

                        $tipoerror = Tipoerror::find($request['tipo_error']);
                        $dataSoporte['tipoerror'] = $tipoerror->nombre;
                        $dataSoporte['caso'] = $case;

                        //$emailSoporte = (intval(Auth::user()->cc_id) === 3) ? 'info@linkiados.com' : 'soporte@linkiados.com';
                        $emailSoporte = 'info@linkiados.com';
                        
                        $emailAdminLinkiados = new EmailLinkiados([$emailSoporte], 'Soporte linkiados', 'emails.soportelinkiados', $dataSoporte);
                        
                        $this->addImgAttachEmail($infoUploads, $emailAdminLinkiados);
                        $emailAdminLinkiados->send();

                        return response()->json(['message' => 'Soporte almacenado.', 'status' => true]);
                    });
        } catch (Exception $exc) {
            return response()->json(['message' => 'Se presento un error mientras se intentaba almacenar el soporte.', 'status' => false]);
        } catch (\PDOException $exc) {
            return response()->json(['message' => 'Se presento un error mientras se intentaba almacenar el soporte.', 'status' => false]);
        }
    }

    private function addImgAttachEmail($infoUploads, $email) {
        foreach ($infoUploads as $key => $value) {
            $email->addAttachImg("storage/app/public/$value", $key);
        }
    }

    private function uploadFiles($request) {
        $array_uploads = array();
        foreach ($request->all() as $key => $value) {
            if ($request->hasFile($key)) {
                $attach = $request->file($key);
                $nameFile = $attach->getClientOriginalName();
                $urlAttach = $request->file($key)->store('soporte', 'public');
                $array_uploads["$nameFile"] = $urlAttach;
            }
        }

        return $array_uploads;
    }

}
