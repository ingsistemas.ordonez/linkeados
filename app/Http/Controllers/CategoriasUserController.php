<?php

namespace App\Http\Controllers;

use App\CategoriasComercios;
//use Illuminate\Http\Request;
use App\Http\Requests\CategoriasUserRequest;

class CategoriasUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('categoriasu.index', [
            'categorias' => CategoriasComercios::latest()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('categoriasu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriasUserRequest $request)
    {
        //
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoriasComercios  $categoriasComercios
     * @return \Illuminate\Http\Response
     */
    public function show(CategoriasComercios $categoriasu)
    {
        //
        //dd($categoriasComercios->id);
        return view('categoriasu.view', ['categoria' => $categoriasu]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoriasComercios  $categoriasComercios
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoriasComercios $categoriasComercios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoriasComercios  $categoriasComercios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoriasComercios $categoriasComercios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoriasComercios  $categoriasComercios
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoriasComercios $categoriasComercios)
    {
        //
    }
}
