<?php

namespace App\Http\Controllers;

use App\Bancos;
use App\DatosBancarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DatosBancariosController extends Controller {

    private $uploadRute = '/uploads/constancias/';

    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        $datosBancariosUser = DatosBancarios::where('user_id', Auth::user()->id)->first();
        //$bancos = Bancos::orderBy('nombre')->get();
        return view('datosbancarios.create', ['datosBancariosUser' => $datosBancariosUser]);
    }

    public function createprocess(Request $request) {

        try {
            return DB::transaction(function() use($request) {
                        $infoUploads = $this->uploadFiles($request);

                        $datoBancario = DatosBancarios::where('user_id', Auth::user()->id)->first();

                        if ($datoBancario) {

                            $datoBancario->banco_id = 1;
                            $datoBancario->nombre = $request['nombre'];
                            $datoBancario->cuenta = $request['cuenta'];
                            $datoBancario->tipo = $request['tipo_cuenta'];
                            $datoBancario->correo = $request['correo'];
                            $datoBancario->fecha_pago = $request['fecha_pago'];
                            $datoBancario->telefono = $request['telefono'];
                            $datoBancario->direccion = $request['direccion'];
                            $datoBancario->banco_nombre = $request['banco_nombre'];

                            if (isset($infoUploads['namefile'])) {
                                $datoBancario->constancia_activa = $infoUploads['namefile'];
                                $datoBancario->constancia_activa_url = $infoUploads['file'];
                            }

                            $datoBancario->save();
                        } else {
                            $datoBancario = DatosBancarios::create([
                                        'user_id' => Auth::user()->id,
                                        'banco_id' => 1,
                                        'nombre' => $request['nombre'],
                                        'cuenta' => $request['cuenta'],
                                        'tipo' => $request['tipo_cuenta'],
                                        'constancia_activa' => $infoUploads['namefile'],
                                        'constancia_activa_url' => $infoUploads['file'],
                                        'correo' => $request['correo'],
                                        'fecha_pago' => $request['fecha_pago'],
                                        'telefono' => $request['telefono'],
                                        'direccion' => $request['direccion'],
                                        'banco_nombre' => $request['banco_nombre']
                            ]);
                        }

                        return response()->json(['message' => 'Dato bancario almacenado.', 'status' => true]);
                    });
        } catch (Exception $exc) {
            return response()->json(['message' => 'Se presento un error mientras se intentaba almacenar el dato bancario.', 'status' => false]);
        } catch (\PDOException $exc) {
            return response()->json(['message' => 'Se presento un error mientras se intentaba almacenar el dato bancario.', 'status' => false]);
        }
    }

    private function uploadFiles($request) {
        $array_uploads = array();
        foreach ($request->all() as $key => $value) {
            if ($request->hasFile($key)) {
                $attach = $request->file($key);
                $urlAttach = time() . '-' . rand(0, 1000) . '.' . $attach->extension();
                $attach->move(public_path() . $this->uploadRute, $urlAttach);
                $array_uploads["name$key"] = $attach->getClientOriginalName();
                $array_uploads["$key"] = $urlAttach;
            }
        }

        return $array_uploads;
    }

    public function show($users)
    {
        $datosBancariosUser = DatosBancarios::where('user_id', $users)->first();
        return view('datosbancarios.view', ['datos' => $datosBancariosUser]);
    }

}
