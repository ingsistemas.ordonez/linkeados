<?php

namespace App\Http\Services;

use SendGrid\Mail\Mail;

class EmailLinkiados {

    private $view;
    private $arrayTo;
    private $model;
    private $subject;
    private $emailFrom = 'info@linkiados.com';
    private $nameFrom = 'Info Linkiados';
    private $email;

    public function __construct($arrayTo, $subject, $view, $model) {
        $this->view = $view;
        $this->arrayTo = $arrayTo;
        $this->subject = $subject;
        $this->model = $model;

        $this->generateEmail();
    }

    private function generateEmail() {
        $this->email = new Mail();
        $this->email->setFrom($this->emailFrom, $this->nameFrom);
        $this->email->setSubject($this->subject);

        foreach ($this->arrayTo as $to) {
            $this->email->addTo($to);
        }

        $this->email->addContent("text/html", $this->getContentView());
    }

    public function setFrom($email, $name) {
        $this->emailFrom = $email;
        $this->nameFrom = $name;
    }

    public function addAttachImg($url, $name) {
        $file_encoded = base64_encode(file_get_contents($url));
        $this->email->addAttachment($file_encoded, "image/jpeg", $name, "attachment");
    }

    public function send() {
        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($this->email);
            return $response->statusCode();
        } catch (Exception $e) {
            return 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }

    public function getView() {
        return $this->view;
    }

    public function getArrayTo() {
        return $this->arrayTo;
    }

    public function getModel() {
        return $this->model;
    }

    public function getSubject() {
        return $this->subject;
    }

    private function getContentView() {
        if ($this->view) {
            return view($this->view, ['model' => $this->model])->toHtml();
        }
        return '';
    }

}
