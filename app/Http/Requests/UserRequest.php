<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Hash;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {               
                return [
                    'name'      => 'required',
                    //'email'     => 'required|email|unique:users,email,'.$this->user->id,
                    'email'     => 'required|email|unique:users,email',
                    'password'  => ['required','min:6'],
                    //'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                    //'password_confirmation' => 'min:6',
                    'url_perfil'=> 'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'      => 'required',
                    'email'     => 'required|email|unique:users,email,'.$this->user->id,
                    //'password'  => 'min:6',
                    //'password_confirmation' => 'min:6',
                    //'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                    //'password' => 'nullable|string|confirmed',
                    //'password_confirmation' => 'required',
                    //'url_perfil'    => 'required'
                ];
                /*return [                    
                    'name' => 'unique:permissions,name,'.$this->get('id').'|max:255',
                    'display_name' => 'required',
                ];*/
            }
            default:break;
        }    
    }

     /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        /*$validator->after(function ($validator) {
            if(!empty( $this->password)) {
                if ( $this->password_confirmation != $this->password ) {
                    $validator->errors()->add('password', 'Las contraseñas deben ser iguales.');
                }
            }
        });
        return;*/
    }
}
