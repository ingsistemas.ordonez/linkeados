<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * 'nombre_p', 'web', 'url_web', 'descripcion', 'porcentaje_a', 'cp_id', 'codigo', 'users_id', 'valor', 'conversion'
     * @return array
     */
    public function rules()
    {
        if(Auth::user()->cc_id == 3) { //rol administrador
            return [
                'nombre_p' => 'required',
                //'web' => 'required',
                'url_web' => 'required',
                'sku' => 'required',
                'descripcion' => 'required',
                'cp_id' => 'required',
                'valor' => 'required',
                'porcentaje_a' => 'required'
            ];
        } else {
            return [
                'nombre_p' => 'required',
                //'web' => 'required',
                'url_web' => 'required',
                'descripcion' => 'required',
                'cp_id' => 'required',
                'valor' => 'required',
                'sku' => 'required',
                'conversion' => 'required',
                'porcentaje_a' => 'required'
            ];
        }
    }
}
