<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopify extends Model
{
    protected $table = 'shopify';

    protected $fillable = [ 
        'llave', 'url_tienda', 'users_id', 'nombre', 'verificado'
    ]; 
}
