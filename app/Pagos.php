<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Pagos extends Model
{
    //
    protected $fillable = ['codigo_pago', 'users_id', 'links_id', 'productos_id', 'comision_afiliado','comision_admin','estado_comerciante','estado_afiliado']; 

    protected $hidden = [];

    public function users()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function producto()
    {
        $producto = $this->belongsTo(Productos::class, 'productos_id');
        if($producto)
            return $producto;
        else   
            return 'Producto Eliminado';
    }

    public function getNombreusuarioAttribute()  
    {
        $producto = Productos::
        where('id', '=', $this->productos_id)
        ->first();

        if($producto) {
            return $producto->users_id;
        } else { 
            return 'N/A'; 
        }
    }

    public function getProductoNombreAttribute()
    {
        $db = Productos::
        where('id', '=', $this->productos_id)
        ->first(); 

        if($db)
            return $db->nombre_p;
        else   
            return 'Producto Eliminado';
    }

    public function getTipoAttribute()
    {
        $db = CategoriasComercios::
        where('id', '=', $this->users->cc_id)
        ->first(); 

        return $db->nombre_cc; 
    }

    public function getFormatFechaAttribute() {
        return $this->created_at->format('d M Y');
    }

    public function getEstadoAfiliadoDinamicoAttribute() {
        switch($this->estado_afiliado){
            case 'E':
                $nombre = 'Espera';
            break;
            case 'EA':
                $nombre = 'Espera Ad';
            break;
            case 'R':
                $nombre = 'Rechazado';
            break;
            case 'C':
                $nombre = 'Completado';
            break;
        }

        return '<div class="box-estado box-'.$this->estado_afiliado.'">
                    <div style="display:block">'.$nombre.'</div>
                </div>';
    }

    public function getEstadoComercioDinamicoAttribute() {
        switch($this->estado_comerciante){
            case 'E':
                $nombre = 'Espera';
            break;
            case 'EA':
                $nombre = 'Espera Ad';
            break;
            case 'R':
                $nombre = 'Rechazado';
            break;
            case 'C':
                $nombre = 'Completado';
            break;
        }

        return '<div class="box-estado box-'.$this->estado_comerciante.'">
                    <div style="display:block">'.$nombre.'</div>
                </div>';
    }

    public function getEstadosPagosAttribute() {
        $estados = [
            'E' => 'Espera',
            'EA' => 'Espera Admin',
            'R' => 'Rechazado',
            'C' => 'Completado'
        ];

        return $estados;    
    }    

    public function sumaTotal()
    {
        return  $this->comision_admin + $this->comision_afiliado; 
    }
}
