<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soporte extends Model {

    protected $table = 'soporte';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'tipoerror_id', 'nombre', 'comentario', 'adjunto', 'telefono', 'correo', 'sitio_web'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function tipoerror() {
        return $this->belongsTo(Tipoerror::class);
    }

    public function adjuntos() {
        return $this->hasMany(SoporteAdjuntos::class);
    }

}
