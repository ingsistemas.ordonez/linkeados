<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Auth::routes();

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/registerc', 'Auth\RegisterController@registerc')->name('register.registerc');
//Route::resource('users', 'UserController')->middleware('auth');
//Route::get('categoriasu', 'CategoriasUserController@index');

Route::get('recuperarpassword/{hash}', 'Auth\ResetPasswordController@resetpassword')->name('user.resetpassword');
Route::post('enviarlink/password', 'Auth\ResetPasswordController@resetpasswordhash')->name('usuario.sendlinkpassword');
Route::post('actualizar/password', 'Auth\ResetPasswordController@updatepassword')->name('usuario.updatepassword');

Route::middleware(['auth'])->group(function () {

    /* Route::get('inicio', function () {
      return view('welcome');
      })->name('inicio'); */

    Route::resource('users', 'UserController'); //CRUD
    Route::resource('categoriasu', 'CategoriasUserController'); //CRUD s
    Route::resource('productos', 'ProductoController'); //CRUD
    Route::resource('links', 'LinkController'); //CRUD

    Route::post('productos/{producto}/duplicate', 'ProductoController@duplicate')->name('productos.duplicate');

    Route::post('productos/{producto}/linkear', 'ProductoController@linkear')->name('productos.linkear');

    Route::resource('configuracion', 'ConfiguracionController'); //CRUD

    Route::get('cargamasiva', 'CargaMasivaController@index')->name('cargamasiva.index');
    Route::get('cargamasiva/plantilla', 'CargaMasivaController@downloadtemplate')->name('cargamasiva.template');
    Route::post('cargamasiva/procesar', 'CargaMasivaController@process')->name('cargamasiva.proccess');

    Route::get('cargamasiva/users', 'CargaMasivaController@indexusers')->name('userscargamasiva.index');
    Route::get('cargamasiva/plantilla/usuarios', 'CargaMasivaController@downloadtemplateusers')->name('cargamasivausers.template');
    Route::post('cargamasiva/procesar/usuarios', 'CargaMasivaController@processusers')->name('cargamasivausers.proccess');

    Route::get('soporte/create', 'SoporteController@create')->name('soporte.create');
    Route::post('soporte/create/process', 'SoporteController@createprocess')->name('soporte.createprocess');

    Route::get('datosbancarios', 'DatosBancariosController@create')->name('datosbancarios.create');
    Route::post('datosbancarios/create/procesar', 'DatosBancariosController@createprocess')->name('datosbancarios.createprocess');
    Route::get('datosbancarios/{users}', 'DatosBancariosController@show')->name('datosbancarios.show');

    //Route::get('users', 'UserController@linkpago')->name('users.linkpago');

    Route::get('perfil/edit', 'PerfilController@edit')->name('perfil.edit');
    Route::post('perfil/edit/process', 'PerfilController@editprocess')->name('perfil.editprocess');

    Route::post('perfil/edit/setshopify', 'PerfilController@setshopify')->name('perfil.setshopify');

    Route::post('users/{user}/updateurl', 'UserController@updateurl')->name('users.updateurl');

    Route::get('pagos/', 'PagosController@index')->name('pagos.index');
    Route::post('pagos/spa', 'PagosController@spa')->name('pagos.spa');
    Route::post('pagos/spl', 'PagosController@spl')->name('pagos.spl');
    Route::get('pagos/{pagos}/edit', 'PagosController@edit')->name('pagos.edit');
    Route::put('pagos/{pagos}/update', 'PagosController@update')->name('pagos.update');
    //Route::get('categoriasComercios/{cc}', 'CategoriasUserController@show')->name('categoriasComercios');

    Route::post('categoriaproducto/create', 'CategoriascController@create')->name('categoriaproductos.create');
    Route::post('categoriaproducto/eliminar', 'CategoriascController@eliminar')->name('categoriaproductos.delete');
});

//Route::resource('users', 'UserController');
//Route::get('users','UserController@index');
//Route::post('users','UserController@store')->name('users.store');
//Route::delete('users/{user}','UserController@destroy')->name('users.destroy');





