<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//->middleware('auth:api');

Route::group(['middleware' => ['cors']], function () {
    
    //JS
    Route::post('linkeados/visto', 'Api\LinkeadosController@visto')->name('linkeados.visto');
    Route::post('tiendalinkeado', 'Api\LinkeadosController@tiendalinkeado')->name('linkeados.tiendalinkeado');

    //Webhook Shopify
    Route::post('/webhook', 'Api\LinkeadosController@index')->name('index');
    Route::post('/webhook/tiendashopify', 'Api\LinkeadosController@tiendashopify')->name('tiendashopify');
    Route::post('/webhook/pagoshopify', 'Api\LinkeadosController@pagoshopify')->name('pagoshopify');
    Route::post('/webhook/newproducto', 'Api\LinkeadosController@newproducto')->name('newproducto');

    /*Route::post('compralinkeado', function(Request $request) {
        return $request->all;
    });*/
    //Route::resource('linkeados', 'Api\LinkeadosController'); //CRUD
    //Rutas a las que se permitirá acceso
});