<?php
    
    error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ); 
    ini_set("display_errors", 1);

    header('Content-Type: text/html; charset=utf-8');
    
    header('Access-Control-Allow-Origin: *');
    // Specify which request methods are allowed
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    // Additional headers which may be sent along with the CORS request
    // The X-Requested-With header allows jQuery requests to go through
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    // headers to tell that result is JSON
    header('Content-type: application/json');
    
    //$data = json_decode(file_get_contents('php://input'), true);

    header("Status: 200 Found");
    
    echo json_encode(
        array( 
            'response'=>'hola desde linkeados'
        ), JSON_UNESCAPED_UNICODE
    );
?>