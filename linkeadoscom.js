// Your web app's Linkeados configuration
/*var linkeadoConfig = {
    vendorEmail: "ingsistemas.ordonez@gmail.com ", 
    urlpay: "https://www.kamgus.com/success",
};*/

let ipUser = 0;
function getIP(json) {
    ipUser = json.ip;
    console.log("My public IP address is Ipuser: ", ipUser);
    localStorage.setItem('ipuser', ipUser);
}

// self executing function here
function linkeadosApp(parametros) {
    // your page initialization code here
    // the DOM will be available here
    console.log("Entro a linkeados");
    const codigo = getParam("codigo");
    const url_current = document.URL;

    if (codigo != null) {
        // Fetch preflight request
        let productos = [];

        if(localStorage.getItem("codigo_linkieados") !== null) {

            var guardado = localStorage.getItem('codigo_linkieados');
            let existeC = false;

            JSON.parse(guardado).forEach((item) => {
                productos.push(item);
                if(item == codigo) 
                    existeC = true;
            });

            setTimeout(()=>{
                //if(!existeC) {
                    var request = createCORSRequest(
                        "POST",
                        "https://app.linkiados.com/api/linkeados/visto"
                    );
                    
                    if (request) {
                            var data = new FormData();
                            data.append("codigo", codigo);
                            data.append("ipuser", localStorage.getItem("ipuser"));
                            // Define a callback function
                            request.onload = function () {};
                            // Send request
                            request.send(data);
                    }
                    productos.push(codigo);
                //}
                    
                localStorage.setItem("codigo_linkieados",  JSON.stringify(productos));
            },300);

        } else {

            setTimeout(()=>{
                var request = createCORSRequest(
                    "POST",
                    "https://app.linkiados.com/api/linkeados/visto"
                );
                if (request) {
                    var data = new FormData();
                    data.append("codigo", codigo);
                    data.append("ipuser", localStorage.getItem("ipuser"));
                    // Define a callback function
                    request.onload = function () {};
                    // Send request
                    request.send(data);
                }
                
                productos.push(codigo);
                localStorage.setItem("codigo_linkieados",  JSON.stringify(productos));
            },300);
        }

    } else if (localStorage.getItem("codigo_linkieados") != null) {

        //localStorage.removeItem("codigo_linkieados");

        /*var request = createCORSRequest(
            "POST",
            "https://app.linkiados.com/api/tiendalinkeado"
        );
        if (request) {
            var data = new FormData();
            //data.append("codigo", localStorage.getItem("codigo_linkieados"));
            JSON.parse(localStorage.getItem("codigo_linkieados")).forEach((item) => {
                data.append('codigos[]', item);
            });

            data.append("ipuser", localStorage.getItem("ipuser"));
            data.append("url_current", url_current);
            data.append("urlpago", parametros.urlpay);

            request.onload = function () {
                localStorage.removeItem("codigo_linkieados");
            };
            // Send request
            request.send(data);
        }*/
    }
}

function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR has 'withCredentials' property only if it supports CORS
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        // if IE use XDR
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        xhr = null;
    }
    return xhr;
}

function getParam(param) {
    return new URLSearchParams(window.location.search).get(param);
}
